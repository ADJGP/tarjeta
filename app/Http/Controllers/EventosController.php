<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Evento;
use App\TipoEvento;
use App\Periocidad;

class EventosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $eventos = Evento::with('periocidad','tipo_evento')->get();
        $tipo = TipoEvento::where('status', 1)->get();
        $periodos = Periocidad::where('status', 1)->get();
        return view('eventos.index', compact('periodos', 'tipo', 'eventos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validamos que los datos cumplan con los requisitos
        $validar = Validator::make(
            $request->all(),
            ['name' => 'required|unique:eventos|min:3', 'tipo' => 'required', 'inicio' => 'required', 'final' => 'required', 'cupos' => 'required', 'periocidad' => 'required'],
            ['required' => 'Campo Requerido', 'unique' => 'Ya existe un evento con ese nombre', 'min' => 'Longitud minima permitida 3 caracteres']
        );

        //Si existen errores
        if (count($validar->errors()) > 0) {

            //Retornamos cada uno de los errores
            return response()->json(['status' => 500, 'errors' => $validar->errors()]);
        }
        //Si no creamos el registro
        else {


            $evento = new Evento();
            $evento->name = $request->name;
            $evento->tipo_evento_id = $request->tipo;
            $evento->cupos = $request->cupos;
            $evento->fec_ini = $request->inicio;
            $evento->fec_fin = $request->final;
            $evento->periocidad_id = $request->periocidad;
            if (!$request->observacion) {

                $evento->observacion = "Sin Observaciones";
              }else{ 
                $evento->observacion = $request->observacion; 
              }
            $evento->save();


            Session::flash('message', 'Se ha registrado el evento: ' . $evento->name);
            Session::flash('class', 'success');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $evento = Evento::find($id);
        $name = $evento->name;
        $evento->delete();

        Session::flash('message', 'Se ha eliminado el evento: ' . $name);
        Session::flash('class', 'danger');
    }

    public function listado()
    {   
        $eventos = Evento::with('periocidad','tipo_evento')->get();
        //$tipo = TipoEvento::where('status', 1)->get();
        //$periodos = Periocidad::where('status', 1)->get();
        return view('miembros.listado', compact( 'eventos'));
    }
}
