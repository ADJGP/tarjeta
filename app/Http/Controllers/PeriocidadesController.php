<?php

namespace App\Http\Controllers;

use App\Periocidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PeriocidadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $periocidades = Periocidad::all();
       return view('periocidades.index', compact('periocidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate(['name' => 'required|min:4|unique:periocidades'],['name.required' => 'Campo Requerido', 'name.min' => 'Longitud Minima de 4 Caracteres', 'name.unique' => 'Este periodo ya existe, verifique el listado']);

        $Periocidad = new Periocidad();
        $Periocidad->name = $request->name;
        $Periocidad->save();

        Session::flash('message','Se ha creado la Periocidad: '.$request->name.'!' );
        Session::flash('class','success');

        return redirect('/periocidades');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cambiar_status($id)
    {

        /*Consultamos al periodo a modificar*/
        $periodo = Periocidad::find($id);
        
        /*Si al periodo esta activada => desactivala*/
        if ($periodo->status == 1) {
            
            $periodo->status = 0;
            $periodo->save();

            Session::flash('message', 'Se ha desactivado al periodo:'.' '.$periodo->name);
            Session::flash('class', 'danger');

        /*Si la periodo esta desactivada => activada*/
        }else if ($periodo->status == 0) {
            
            $periodo->status = 1;
            $periodo->save();

            Session::flash('message', 'Se ha activado al periodo:'.' '.$periodo->name);
            Session::flash('class', 'info');    
        }

    }
}
