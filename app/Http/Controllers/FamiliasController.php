<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Familia;
use App\Integrante;
use DB;
use Illuminate\Support\Facades\Auth;

class FamiliasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $familias = Familia::all();
        return view('familias.index', compact('familias','integrantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $fam = new Familia();
        $fam->cod_familia = uniqid('Fam_',true);
        $fam->save();

        Session::flash('message', 'Se ha Creado una Familia con el Codigo: '.$fam->cod_familia.'!');
        Session::flash('class', 'success');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $f = Familia::find($id);
         $cod = $f->cod_familia;
         $f->delete();

        Session::flash('message', 'Familia: '.$cod.' Eliminada Exitosamente!');
        Session::flash('class', 'success');

        //return redirect()->back();

    }

    public function cargar($id)
    {
          $integrantes = Integrante::where('cod_familia',NULL)->where('status',1)->get();

          $num = count($integrantes);

          if ($num>0) {

             return $integrantes;

          }else{

            return 0;
          }
   
    }

    public function anadir($id,$familia)
    {

          $int = Integrante::find($id);
          $int->cod_familia = $familia;
          $int->save(); 
    }

    public function ver($id)
    {

          $ints = Integrante::where('cod_familia',$id)->get();
          
          $num = count($ints);

          if ($num>0) {

             return $ints;

          }else{

            return 0;
          }
  
    }

      public function salir($id)
    {

          $int = Integrante::find($id);
          $int->cod_familia = NULL;
          $int->save();

    }

    public function familia_user()
    {    
        //Colsuto si el usuario posee familia asignada
        $f = Auth::user()->integrante->cod_familia;
        
        //Si el valor es null retorno false
        if($f===null){

            $family = 0;
            $familias = Familia::all();

            return view('miembros.familia', compact('family','familias'));

        }else{
             
            $family= 1;
            $result = Familia::where('cod_familia',$f)->first();
            return view('miembros.familia', compact('family','result'));
        }
          
          //return dd($familia);
          

    }
}
