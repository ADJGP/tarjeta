<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Empresa;
use App\Evento;
use PHPUnit\Framework\Constraint\Count;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        //Conteo de usuarios odviando el admin 
        $users = Count(User::where('rol_id','<>',0)->get());

        //Conteo de empresas
        $empresas = Count(Empresa::all());

        //Conteo de eventos
        $events = Count(Evento::all());

        $eventos = Evento::with('tipo_evento')->get();

        return view('home', compact('users','empresas','events','eventos'));
    }
}
