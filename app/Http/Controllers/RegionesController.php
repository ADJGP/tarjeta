<?php

namespace App\Http\Controllers;

use App\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RegionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $regiones = Region::all();
       return view('regiones.index', compact('regiones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate(['name' => 'required|min:4|unique:regiones'],['name.required' => 'Campo Requerido', 'name.min' => 'Longitud Minima de 4 Caracteres', 'name.unique' => 'Esta Region ya existe, verifique el listado']);

        $regiones = new Region();
        $regiones->name = $request->name;
        $regiones->save();

        Session::flash('message','Se ha creado la Region: '.$request->name.'!' );
        Session::flash('class','success');

        return redirect('/regiones');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function cambiar_status($id)
    {

        /*Consultamos la region a modificar*/
        $region = Region::find($id);
        
        /*Si la region esta activada => desactivala*/
        if ($region->status == 1) {
            
            $region->status = 0;
            $region->save();

            Session::flash('message', 'Se ha desactivado a la region:'.' '.$region->name);
            Session::flash('class', 'danger');

        /*Si la region esta desactivada => activada*/
        }else if ($region->status == 0) {
            
            $region->status = 1;
            $region->save();

            Session::flash('message', 'Se ha activado a la region:'.' '.$region->name);
            Session::flash('class', 'info');    
        }

    }
}
