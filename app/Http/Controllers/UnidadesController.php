<?php

namespace App\Http\Controllers;

use App\Unidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UnidadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $unidades = Unidad::all();
       return view('unidades.index', compact('unidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['name' => 'required|min:4|unique:unidades'],['name.required' => 'Campo Requerido', 'name.min' => 'Longitud Minima de 4 Caracteres', 'name.unique' => 'Esta Unidad ya existe, verifique el listado']);

        $Unidades = new Unidad();
        $Unidades->name = $request->name;
        $Unidades->save();

        Session::flash('message','Se ha creado la Unidad: '.$request->name.'!' );
        Session::flash('class','success');

        return redirect('/unidades');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function cambiar_status($id)
    {

        /*Consultamos la unidad a modificar*/
        $unidad = Unidad::find($id);
        
        /*Si la unidad esta activada => desactivala*/
        if ($unidad->status == 1) {
            
            $unidad->status = 0;
            $unidad->save();

            Session::flash('message', 'Se ha desactivado a la unidad:'.' '.$unidad->name);
            Session::flash('class', 'danger');

        /*Si la unidad esta desactivada => activada*/
        }else if ($unidad->status == 0) {
            
            $unidad->status = 1;
            $unidad->save();

            Session::flash('message', 'Se ha activado a la unidad:'.' '.$unidad->name);
            Session::flash('class', 'info');    
        }

    }
}
