<?php

namespace App\Http\Controllers;

use App\EstadoCivil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class EstadosCivilsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $estadosCiviles = EstadoCivil::all();
       return view('estados_civiles.index', compact('estadosCiviles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['name' => 'required|min:4|unique:estados_civils'],['name.required' => 'Campo Requerido', 'name.min' => 'Longitud Minima de 4 Caracteres', 'name.unique' => 'Este Estado Civil ya existe, verifique el listado']);

        $EstadoCivil = new EstadoCivil();
        $EstadoCivil->name = $request->name;
        $EstadoCivil->save();

        Session::flash('message','Se ha creado el Estado Civil: '.$request->name.'!' );
        Session::flash('class','success');

        return redirect('/estados_civiles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function cambiar_status($id)
    {

        /*Consultamos el Estado Civil a modificar*/
        $estado_civil = EstadoCivil::find($id);
        
        /*Si el Estado Civil esta activada => desactivala*/
        if ($estado_civil->status == 1) {
            
            $estado_civil->status = 0;
            $estado_civil->save();

            Session::flash('message', 'Se ha desactivado a el Estado Civil:'.' '.$estado_civil->name);
            Session::flash('class', 'danger');

        /*Si el Estado Civil esta desactivada => activada*/
        }else if ($estado_civil->status == 0) {
            
            $estado_civil->status = 1;
            $estado_civil->save();

            Session::flash('message', 'Se ha activado a el Estado Civil:'.' '.$estado_civil->name);
            Session::flash('class', 'info');    
        }

    }

}
