<?php

namespace App\Http\Controllers;

use App\Etnia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class EtniasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $etnias = Etnia::all();
       return view('etnias.index', compact('etnias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['name' => 'required|min:4|unique:etnias'],['name.required' => 'Campo Requerido', 'name.min' => 'Longitud Minima de 4 Caracteres', 'name.unique' => 'Esta Etnia ya existe, verifique el listado']);

        $etnias = new Etnia();
        $etnias->name = $request->name;
        $etnias->save();

        Session::flash('message','Se ha creado el Etnia: '.$request->name.'!' );
        Session::flash('class','success');

        return redirect('/etnias');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function cambiar_status($id)
    {

        /*Consultamos la etnia a modificar*/
        $etnia = Etnia::find($id);
        
        /*Si la etnia esta activada => desactivala*/
        if ($etnia->status == 1) {
            
            $etnia->status = 0;
            $etnia->save();

            Session::flash('message', 'Se ha desactivado a la etnia:'.' '.$etnia->name);
            Session::flash('class', 'danger');

        /*Si la etnia esta desactivada => activada*/
        }else if ($etnia->status == 0) {
            
            $etnia->status = 1;
            $etnia->save();

            Session::flash('message', 'Se ha activado a la etnia:'.' '.$etnia->name);
            Session::flash('class', 'info');    
        }

    }
}
