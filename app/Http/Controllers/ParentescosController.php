<?php

namespace App\Http\Controllers;

use App\Parentesco;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ParentescosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $parentescos = Parentesco::all();
       return view('parentescos.index', compact('parentescos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['name' => 'required|min:4|unique:parentescos'],['name.required' => 'Campo Requerido', 'name.min' => 'Longitud Minima de 4 Caracteres', 'name.unique' => 'Este parentesco ya existe, verifique el listado']);

        $parentescos = new Parentesco();
        $parentescos->name = $request->name;
        $parentescos->save();

        Session::flash('message','Se ha creado el Parentesco: '.$request->name.'!' );
        Session::flash('class','success');

        return redirect('/parentescos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function cambiar_status($id)
    {

        /*Consultamos la parentesco a modificar*/
        $parentesco = Parentesco::find($id);
        
        /*Si la parentesco esta activada => desactivala*/
        if ($parentesco->status == 1) {
            
            $parentesco->status = 0;
            $parentesco->save();

            Session::flash('message', 'Se ha desactivado al parentesco:'.' '.$parentesco->name);
            Session::flash('class', 'danger');

        /*Si la parentesco esta desactivada => activada*/
        }else if ($parentesco->status == 0) {
            
            $parentesco->status = 1;
            $parentesco->save();

            Session::flash('message', 'Se ha activado al parentesco:'.' '.$parentesco->name);
            Session::flash('class', 'info');    
        }

    }
}
