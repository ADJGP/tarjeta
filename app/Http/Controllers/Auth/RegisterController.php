<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Integrante;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;
use App\Mail\registroEmail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'rol_id' => ['required'],
            'rut' => ['required','string'],
            'nombres' => ['required', 'string', 'max:255'],
            'apellidos' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        $integrante_New=Integrante::create([
           'rut' => $data['rut'],          
           'nombres' => $data['nombres'],          
           'apellidos' => $data['apellidos'],                 
        ]);

        /*$objDemo = new \stdClass();
        $objDemo->usuario = $data['email'];
        $objDemo->clave = $data['rut'];
        $objDemo->sender = 'Comunidad .::CASTRO::.';
        $objDemo->receiver = $data['nombres'] . ' ' . $data['apellidos'];

        Mail::to($data['email'])->send(new registroEmail($objDemo));*/


        return User::create([
            'rol_id' => $data['rol_id'],
            'integrante_id' => $integrante_New->id,
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
