<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoEvento;
use Illuminate\Support\Facades\Session;
use App\Evento;

class TipoEventosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos = TipoEvento::all();
        return view('tipo_eventos.index', compact('tipos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['name' => 'required|min:5|string|unique:tipo_eventos'], ['required' => 'Campo Requerido', 'min' => 'Longitud Minima 3 Caracteres', 'unique' => 'Ya existe un registro con este nombre']);

        $te = new TipoEvento;
        $te->name = $request->name;
        $te->save();

        $request->session()->flash('message', 'Se ha registrado el tipo de evento ' . $te->name);
        $request->session()->flash('class', 'success');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eventos = Count(Evento::where('tipo_evento_id', $id));

        if ($eventos > 0) {

            Session::flash('message', 'Disculpa en este momento hay eventos asociados a este registro, 
        elimine los eventos para poder eliminar');
            Session::flash('class', 'info');
        } else {


            $tipo = TipoEvento::find($id);
            $name = $tipo->name;
            $tipo->delete();

            Session::flash('message', 'Se ha eliminado al tipo de evento:' . ' ' . $name);
            Session::flash('class', 'danger');
        }
    }

    public function cambiar_status($id)
    {

        /*Consultamos el tipo a modificar*/
        $tipo = TipoEvento::find($id);

        /*Si el tipo esta activada => desactivala*/
        if ($tipo->status == 1) {

            $tipo->status = 0;
            $tipo->save();

            Session::flash('message', 'Se ha desactivado al tipo de evento:' . ' ' . $tipo->name);
            Session::flash('class', 'warning');

            /*Si el tipo esta desactivada => activada*/
        } else if ($tipo->status == 0) {

            $tipo->status = 1;
            $tipo->save();

            Session::flash('message', 'Se ha activado al tipo de evento:' . ' ' . $tipo->name);
            Session::flash('class', 'info');
        }
    }
}
