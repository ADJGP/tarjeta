<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Integrante;
use App\EstadoCivil;
use App\Etnia;
use App\Etario;
use App\Nacionalidad;
use App\Parentesco;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\registroEmail;
use Malahierba\ChileRut\Facades\ChileRut;

class IntegrantesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estadociviles = EstadoCivil::all();
        $etnias = Etnia::all();
        $etarios = Etario::all();
        $nacionalidades = Nacionalidad::all();
        $parentescos = Parentesco::all();
        $integrantes = Integrante::all();
        return view('integrantes.index', compact('integrantes', 'estadociviles', 'etnias', 'etarios', 'nacionalidades', 'parentescos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $integrantes = Integrante::all();
        $estadociviles = EstadoCivil::all();
        $etnias = Etnia::all();
        $etarios = Etario::all();
        $nacionalidades = Nacionalidad::all();
        $parentescos = Parentesco::all();

        return view('integrantes.create', compact('integrantes', 'estadociviles', 'etnias', 'etarios', 'nacionalidades', 'parentescos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['nombres' => 'required|string|max:255', 'apellidos' => 'required|string|max:255', 'rut' => 'required|unique:integrantes', 'fecha_nacimiento' => 'required', 'sexo' => 'required', 'estado_civil_id' => 'required', 'etnia_id' => 'required', 'etario_id' => 'required', 'nacionalidad_id' => 'required', 'telf_movil' => 'required', 'email' => 'required|unique:users', 'rol_id' => 'required'], ['required' => 'Campo Requerido', 'max' => 'Has superado el limite de caracteres permitidos.', 'unique' => 'Este RUT ya esta registrado.', 'email.unique' => 'Correo electronico ya registrado']);

        $integrante_New = Integrante::create([
            'rut' => $request->rut,
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'sexo' => $request->sexo,
            'estado_civil_id' => $request->estado_civil_id,
            'telf_movil' => $request->telf_movil,
            'telf_fijo' => $request->telf_fijo,
            'etnia_id' => $request->etnia_id,
            'etario_id' => $request->etario_id,
            'nacionalidad_id' => $request->nacionalidad_id,
            'inscripcion' => $request->inscripcion,
        ]);

        User::create([
            'rol_id' => $request->rol_id,
            'integrante_id' => $integrante_New->id,
            'email' => $request->email,
            'password' => Hash::make($request->rut),
        ]);

        /*$objDemo = new \stdClass();
        $objDemo->usuario = $request->email;
        $objDemo->clave = $request->rut;
        $objDemo->sender = 'Comunidad .::CASTRO::.';
        $objDemo->receiver = $request->nombres . ' ' . $request->apellidos;

        Mail::to($request->email)->send(new registroEmail($objDemo));*/

        Session::flash('message', 'Se ha registrado al vecino:' . ' ' . $integrante_New->rut . ' ' . $integrante_New->nombres . ' ' . $integrante_New->apellidos);
        Session::flash('class', 'success');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $integrante = Integrante::find($id);
        return $integrante;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $integrante = Integrante::find($id);
        $integrante->rut = $request->rut;
        $integrante->nombres = $request->nombres;
        $integrante->apellidos = $request->apellidos;
        $integrante->fecha_nacimiento = $request->fecha_nacimiento;
        $integrante->sexo = $request->sexo;
        $integrante->estado_civil_id = $request->estado_civil_id;
        $integrante->telf_movil = $request->telf_movil;
        $integrante->telf_fijo = $request->telf_fijo;
        $integrante->etnia_id = $request->etnia_id;
        $integrante->etario_id = $request->etario_id;
        $integrante->nacionalidad_id = $request->nacionalidad_id;
        $integrante->save();

        Session::flash('message', 'Se ha Actualizado los Datos del Vecino:' . ' ' . $integrante->rut . ' ' . $integrante->nombres . ' ' . $integrante->apellidos);
        Session::flash('class', 'info');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $integrante = Integrante::find($id); //$id_int = $integrante->id;

        $user = User::where('integrante_id', $integrante->id)->first();

        if ($user) {

            $user->delete();

            $integrante->delete();

            Session::flash('message', 'Se ha eliminado al vecino de la plataforma');
            Session::flash('class', 'danger');
        } else {

            $integrante->delete();

            Session::flash('message', 'Se ha eliminado al vecino de la plataforma');
            Session::flash('class', 'danger');
        }
    }

    public function cambiar_status($id)
    {

        /*Consultamos la integrante a modificar*/
        $integrante = Integrante::find($id);

        /*Si la integrante esta activada => desactivala*/
        if ($integrante->status == 1) {

            $integrante->status = 0;
            $integrante->save();

            Session::flash('message', 'Se ha desactivado al Vecino:' . ' ' . $integrante->rut . ' ' . $integrante->nombres . ' ' . $integrante->apellidos);
            Session::flash('class', 'danger');

            /*Si la integrante esta desactivada => activada*/
        } else if ($integrante->status == 0) {

            $integrante->status = 1;
            $integrante->save();

            Session::flash('message', 'Se ha activado al Vecino:' . ' ' . $integrante->rut . ' ' . $integrante->nombres . ' ' . $integrante->apellidos);
            Session::flash('class', 'info');
        }
    }

    public function getI()
    {
        return Integrante::all();
    }

    public function validar_rut($rut)
    {
        $validar = new ChileRut;

        if ($validar::check($rut)){
               return 1;
        }else{
               return 0;
        }
        

    }
}
