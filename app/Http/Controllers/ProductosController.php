<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use App\Empresa;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas  = Empresa::where('status',1)->get();
        $productos = Producto::where('status',1)->get();
        return view('productos.index', compact('productos','empresas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['empresa_id'=>'required','abreviado' => 'required|string','gloza'=>'required','vigente'=>'required|numeric','stock_inicial' => 'required|numeric','stock_maximo' => 'required|numeric','stock_minimo' => 'required|numeric', 'fec_termino' => 'required'],['required' => 'Campo Requerido', 'numeric' => 'Solo puedes Ingresar Numeros']);

        Producto::create($request->all());

        $request->session()->flash('message', 'Se ha registrado el Producto:'.$request->abreviado.'!');
        $request->session()->flash('class', 'success');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
