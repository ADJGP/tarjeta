<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comuna;
use App\Empresa;


class EmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comunas = Comuna::where('status',1)->get();
        $empresas = Empresa::where('status',1)->get();
        return view('empresas.index', compact('comunas','empresas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['rut'=> 'required|string|unique:empresas', 'razon_social' => 'required|string|max:255','nombre_fantasia' => 'required|string|max:255','direccion'=>'required','telefono' => 'required','contacto' => 'string|max:255', 'telefono_contacto' => 'string|max:255','correo_contacto' => 'string|max:255','comuna_id' => 'required', 'pagina_web' => 'string|max:255'],['required' => 'Campo Requerido','unique' => 'Esta Empresa ya esta Registrada']);

        Empresa::create($request->all());

        $request->session()->flash('message', 'Se ha registrado la Empresa:'.$request->nombre_fantasia.'!');
        $request->session()->flash('class', 'success');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
