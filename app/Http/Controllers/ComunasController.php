<?php

namespace App\Http\Controllers;

use App\Comuna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ComunasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $comunas = Comuna::all();
       return view('comunas.index', compact('comunas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['name' => 'required|min:4|unique:calles'],['name.required' => 'Campo Requerido', 'name.min' => 'Longitud Minima de 4 Caracteres', 'name.unique' => 'Esta Comuna ya existe, verifique el listado']);

        $Comuna = new Comuna();
        $Comuna->name = $request->name;
        $Comuna->save();

        Session::flash('message','Se ha creado la Comuna: '.$request->name.'!' );
        Session::flash('class','success');

        return redirect('/comunas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cambiar_status($id)
    {

        /*Consultamos la comuna a modificar*/
        $comuna = Comuna::find($id);
        
        /*Si la comuna esta activada => desactivala*/
        if ($comuna->status == 1) {
            
            $comuna->status = 0;
            $comuna->save();

            Session::flash('message', 'Se ha desactivado a la comuna:'.' '.$comuna->name);
            Session::flash('class', 'danger');

        /*Si la comuna esta desactivada => activada*/
        }else if ($comuna->status == 0) {
            
            $comuna->status = 1;
            $comuna->save();

            Session::flash('message', 'Se ha activado a la comuna:'.' '.$comuna->name);
            Session::flash('class', 'info');    
        }

    }
}
