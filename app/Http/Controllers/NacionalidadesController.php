<?php

namespace App\Http\Controllers;

use App\Nacionalidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class NacionalidadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $nacionalidades = Nacionalidad::all();
       return view('nacionalidades.index', compact('nacionalidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['name' => 'required|min:4|unique:nacionalidades'],['name.required' => 'Campo Requerido', 'name.min' => 'Longitud Minima de 4 Caracteres', 'name.unique' => 'Este Estado Civil ya existe, verifique el listado']);

        $Nacionalidades = new Nacionalidad();
        $Nacionalidades->name = $request->name;
        $Nacionalidades->save();

        Session::flash('message','Se ha creado la Nacionalidad: '.$request->name.'!' );
        Session::flash('class','success');

        return redirect('/nacionalidades');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function cambiar_status($id)
    {

        /*Consultamos la nacionalidad a modificar*/
        $nacionalidad = Nacionalidad::find($id);
        
        /*Si la nacionalidad esta activada => desactivala*/
        if ($nacionalidad->status == 1) {
            
            $nacionalidad->status = 0;
            $nacionalidad->save();

            Session::flash('message', 'Se ha desactivado a la nacionalidad:'.' '.$nacionalidad->name);
            Session::flash('class', 'danger');

        /*Si la nacionalidad esta desactivada => activada*/
        }else if ($nacionalidad->status == 0) {
            
            $nacionalidad->status = 1;
            $nacionalidad->save();

            Session::flash('message', 'Se ha activado a la nacionalidad:'.' '.$nacionalidad->name);
            Session::flash('class', 'info');    
        }

    }
}
