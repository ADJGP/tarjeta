<?php

namespace App\Http\Controllers;

use App\Poblacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class PoblacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $poblaciones = Poblacion::all();
       return view('poblaciones.index', compact('poblaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['name' => 'required|min:4|unique:poblaciones'],['name.required' => 'Campo Requerido', 'name.min' => 'Longitud Minima de 4 Caracteres', 'name.unique' => 'Este Poblacion ya existe, verifique el listado']);

        $Poblacion = new Poblacion();
        $Poblacion->name = $request->name;
        $Poblacion->save();

        Session::flash('message','Se ha creado la Poblacion: '.$request->name.'!' );
        Session::flash('class','success');

        return redirect('/poblaciones');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cambiar_status($id)
    {

        /*Consultamos la poblacion a modificar*/
        $poblacion = Poblacion::find($id);
        
        /*Si la poblacion esta activada => desactivala*/
        if ($poblacion->status == 1) {
            
            $poblacion->status = 0;
            $poblacion->save();

            Session::flash('message', 'Se ha desactivado a la poblacion:'.' '.$poblacion->name);
            Session::flash('class', 'danger');

        /*Si la poblacion esta desactivada => activada*/
        }else if ($poblacion->status == 0) {
            
            $poblacion->status = 1;
            $poblacion->save();

            Session::flash('message', 'Se ha activado a la poblacion:'.' '.$poblacion->name);
            Session::flash('class', 'info');    
        }

    }
}
