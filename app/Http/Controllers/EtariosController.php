<?php

namespace App\Http\Controllers;

use App\Etario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class EtariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $etarios = Etario::all();
       return view('etarios.index', compact('etarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['name' => 'required|min:4|unique:etarios'],['name.required' => 'Campo Requerido', 'name.min' => 'Longitud Minima de 4 Caracteres', 'name.unique' => 'Este Etario ya existe, verifique el listado']);

        $Etario = new Etario();
        $Etario->name = $request->name;
        $Etario->save();

        Session::flash('message','Se ha creado el Etario: '.$request->name.'!' );
        Session::flash('class','success');

        return redirect('/etarios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function cambiar_status($id)
    {

        /*Consultamos el etario a modificar*/
        $etario = Etario::find($id);
        
        /*Si el etario esta activada => desactivala*/
        if ($etario->status == 1) {
            
            $etario->status = 0;
            $etario->save();

            Session::flash('message', 'Se ha desactivado al etario:'.' '.$etario->name);
            Session::flash('class', 'danger');

        /*Si el etario esta desactivada => activada*/
        }else if ($etario->status == 0) {
            
            $etario->status = 1;
            $etario->save();

            Session::flash('message', 'Se ha activado al etario:'.' '.$etario->name);
            Session::flash('class', 'info');    
        }

    }
}
