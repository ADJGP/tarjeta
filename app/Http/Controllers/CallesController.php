<?php

namespace App\Http\Controllers;

use App\Calle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class CallesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $calles = Calle::all();
       return view('calles.index', compact('calles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['name' => 'required|min:4|unique:calles'],['name.required' => 'Campo Requerido', 'name.min' => 'Longitud Minima de 4 Caracteres', 'name.unique' => 'Esta Calle ya existe, verifique el listado']);

        $calle = new Calle();
        $calle->name = $request->name;
        $calle->save();

        Session::flash('message','Se ha creado la Calle: '.$request->name.'!' );
        Session::flash('class','success');

        return redirect('/calles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cambiar_status($id)
    {

        /*Consultamos la calle a modificar*/
        $calle = Calle::find($id);
        
        /*Si la calle esta activada => desactivala*/
        if ($calle->status == 1) {
            
            $calle->status = 0;
            $calle->save();

            Session::flash('message', 'Se ha desactivado a la calle:'.' '.$calle->name);
            Session::flash('class', 'danger');

        /*Si la calle esta desactivada => activada*/
        }else if ($calle->status == 0) {
            
            $calle->status = 1;
            $calle->save();

            Session::flash('message', 'Se ha activado a la calle:'.' '.$calle->name);
            Session::flash('class', 'info');    
        }

    }
}
