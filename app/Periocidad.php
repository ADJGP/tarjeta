<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periocidad extends Model
{
    protected $table = 'periocidades';

    protected $fillable = ['name'];

}
