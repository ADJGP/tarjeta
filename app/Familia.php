<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Familia extends Model
{
    protected $table = 'familias';

    protected $fillable = ['cod_familia','direccion','numero','blok','depto','unidad_id','manzana','sitio','poblacion_id','comuna_id','calle_id','region_id'];
}
