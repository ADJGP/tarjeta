<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Periocidad;
use App\TipoEvento;

class Evento extends Model
{
    protected $table = 'eventos';

    protected $fillable = ['name', 'tipo_evento_id', 'cupos', 'fec_ini', 'fec_fin', 'observacion', 'periocidad_id'];

    public function periocidad()
    {

        return $this->belongsTo(Periocidad::class, 'periocidad_id');
    }

    public function tipo_evento()
    {

        return $this->belongsTo(TipoEvento::class, 'tipo_evento_id');
    }
}
