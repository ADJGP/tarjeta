<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etario extends Model
{
    protected $table = 'etarios';

    protected $fillable = ['name'];

}
