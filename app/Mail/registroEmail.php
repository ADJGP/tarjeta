<?php
 
namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class registroEmail extends Mailable
{
    use Queueable, SerializesModels;
     
    /**
     * The Registro object instance.
     *
     * @var Registro
     */
    public $registro;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($registro)
    {
        $this->registro = $registro;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.registro');
    }

   /* public function build()
    {
        return $this->from('a@example.com')
                    ->view('mails.registro')
                    ->text('mails.registro_plain')
                    ->with(
                      [
                            'testVarOne' => '1',
                            'testVarTwo' => '2',
                      ])
                      ->attach(public_path('/img').'/logo.png', [
                              'as' => 'logo.png',
                              'mime' => 'image/png',
                      ]);
    }*/
}
