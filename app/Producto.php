<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'productos';

    protected $fillable = ['empresa_id','name','abreviado','gloza','vigente','fec_termino','stock_inicial','stock_maximo','stock_minimo'];
}
