<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table='empresas';

    protected $fillable = ['rut', 'razon_social', 'nombre_fantasia','direccion','telefono','contacto','telefono_contacto','correo_contacto','comuna_id','pagina_web'];
}
