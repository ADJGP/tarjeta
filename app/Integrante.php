<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Integrante extends Model
{
    protected $table = 'integrantes';

    protected $fillable = ['rut','nombres','apellidos','fecha_nacimiento','sexo','estado_civil_id','parentesco_id','telf_movil','telf_fijo','email','etnia_id','etario_id','nacionalidad_id','inscripcion'];


        public function User()
    {
        return $this->hasOne(User::class);
    }
}
