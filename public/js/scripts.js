function eliminar_familia(id) {

    //alert(id);

    $.ajax({
        type: 'DELETE', //metoodo 
        url: 'familias/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {
            //alert(data);
            setTimeout("location.href='familias'", 1000);
        }
    });


}

function lanzar() {
    $('#familia').val(0);
    $('#modal-default').modal('show');
}

function cargar_integrantes() {

    //
    $('#integrantes').empty();

    id = $('#familia').val();

    $.ajax({
        type: 'POST', //metoodo 
        url: 'familias/cargar/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            //alert(data);

            if (data == 0) {

                $('#integrantes').append("<tr><td colspan='4' class='text-center'>Lo Sentimos, No Hay Integrantes Disponibles</td></tr>");


            } else {

                n = 0;
                $.each(data, function (index, val) {

                    n = n + 1;

                    $('#integrantes').append("<tr id='tr-" + val.id + "'><td>" + n + "</td><td>" + val.rut + "</td><td>" + val.nombres + " " + val.apellidos + "</td><td><button class='btn btn-info' onclick='anadir(" + val.id + ")'><i class='fa fa-plus'></i></button></td></tr>");
                });

            }



        }
    });
}

function anadir(id) {

    $('#tr-' + id).fadeOut('slow/400/fast');

    familia = $('#familia').val();

    //alert(familia);

    $.ajax({
        type: 'POST', //metoodo 
        url: 'familias/anadir/' + id + '/' + familia, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            alert('Anadido Exitosamente');

        }
    });

}

function ver_integrantes(id) {

    $('#integrantes2').empty();

    $.ajax({
        type: 'POST', //metoodo 
        url: 'familias/ver/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {
            $('#name-f').html(id);
            $('#modal-default2').modal('show');

            if (data == 0) {

                $('#integrantes2').append("<tr><td colspan='4' class='text-center'>Lo Sentimos, No Se Han Asignado Integrantes</td></tr>");

            }

            n = 0;
            $.each(data, function (index, val) {

                n = n + 1;

                $('#integrantes2').append("<tr id='tr-" + val.id + "'><td>" + n + "</td><td>" + val.rut + "</td><td>" + val.nombres + " " + val.apellidos + "</td><td><button class='btn btn-info' onclick='Salir(" + val.id + ")' title='Salir de la Familia'><i class='fa fa-minus'></i></button></td></tr>");
            });

        }
    });
}

function Salir(id) {

    $('#tr-' + id).fadeOut('slow/400/fast');

    $.ajax({
        type: 'POST', //metoodo 
        url: 'familias/salir/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            alert('Se ha retirado de la Familia');

        }
    });

}

function calle_status(id) {

    $.ajax({
        type: 'POST', //metoodo 
        url: 'calles/cambiar_status/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            setTimeout("location.href='calles'", 1000);

        }
    });

}

function comuna_status(id) {

    $.ajax({
        type: 'POST', //metoodo 
        url: 'comunas/cambiar_status/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            setTimeout("location.href='comunas'", 1000);

        }
    });

}

function ec_status(id) {

    $.ajax({
        type: 'POST', //metoodo 
        url: 'estados_civiles/cambiar_status/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            setTimeout("location.href='estados_civiles'", 1000);

        }
    });

}

function etario_status(id) {

    $.ajax({
        type: 'POST', //metoodo 
        url: 'etarios/cambiar_status/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            setTimeout("location.href='etarios'", 1000);

        }
    });

}

function etnia_status(id) {

    $.ajax({
        type: 'POST', //metoodo 
        url: 'etnias/cambiar_status/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            setTimeout("location.href='etnias'", 1000);

        }
    });

}

function nacionalidad_status(id) {

    $.ajax({
        type: 'POST', //metoodo 
        url: 'nacionalidades/cambiar_status/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            setTimeout("location.href='nacionalidades'", 1000);

        }
    });

}

function parentesco_status(id) {

    $.ajax({
        type: 'POST', //metoodo 
        url: 'parentescos/cambiar_status/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            setTimeout("location.href='parentescos'", 1000);

        }
    });

}

function periodo_status(id) {

    $.ajax({
        type: 'POST', //metoodo 
        url: 'periocidades/cambiar_status/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            setTimeout("location.href='periocidades'", 1000);

        }
    });

}

function poblacion_status(id) {

    $.ajax({
        type: 'POST', //metoodo 
        url: 'poblaciones/cambiar_status/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            setTimeout("location.href='poblaciones'", 1000);

        }
    });

}

function unidad_status(id) {

    $.ajax({
        type: 'POST', //metoodo 
        url: 'unidades/cambiar_status/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            setTimeout("location.href='unidades'", 1000);

        }
    });

}

function region_status(id) {

    $.ajax({
        type: 'POST', //metoodo 
        url: 'regiones/cambiar_status/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            setTimeout("location.href='regiones'", 1000);

        }
    });

}

function tipo_status(id) {

    $.ajax({
        type: 'POST', //metoodo 
        url: 'tipo_eventos/cambiar_status/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            setTimeout("location.href='tipo_eventos'", 1000);

        }
    });

}

function integrante_status(id) {

    $.ajax({
        type: 'POST', //metoodo 
        url: 'integrantes/cambiar_status/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            setTimeout("location.href='integrantes'", 1000);

        }
    });

}

function integrante_edit(id) {

    $('#integrante-edit').modal('show');

    $.ajax({
        type: 'GET', //metoodo 
        url: 'integrantes/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            $('#id').val(data['id']);
            $('#rut').val(data['rut']);
            $('#nombres').val(data['nombres']);
            $('#apellidos').val(data['apellidos']);
            $('#fecha_nacimiento').val(data['fecha_nacimiento']);
            $('#sexo').val(data['sexo']);
            $('#estado_civil_id').val(data['estado_civil_id']);
            $('#telf_movil').val(data['telf_movil']);
            $('#telf_fijo').val(data['telf_fijo']);
            $('#etnia_id').val(data['etnia_id']);
            $('#etario_id').val(data['etario_id']);
            $('#nacionalidad_id').val(data['nacionalidad_id']);

        }
    });

}

function integrante_update() {

    id = $('#id').val();
    $.ajax({
        type: 'PUT', //metoodo
        url: 'integrantes/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $('#id').val(),
            'rut': $('#rut').val(),
            'nombres': $('#nombres').val(),
            'apellidos': $('#apellidos').val(),
            'fecha_nacimiento': $('#fecha_nacimiento').val(),
            'sexo': $('#sexo').val(),
            'estado_civil_id': $('#estado_civil_id').val(),
            'telf_movil': $('#telf_movil').val(),
            'telf_fijo': $('#telf_fijo').val(),
            'etnia_id': $('#etnia_id').val(),
            'etario_id': $('#etario_id').val(),
            'nacionalidad_id': $('#nacionalidad_id').val(),

        },
        success: function (data) {

            setTimeout("location.href='integrantes'", 1000);

        }
    });
}

function integrante_eliminar(id) {

    $.ajax({
        type: 'DELETE', //metoodo 
        url: 'integrantes/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {

            setTimeout("location.href='integrantes'", 1000);

        }
    });

}

function tipo_eliminar(id) {

    if (confirm('Desea eliminar este registro?')) {

        $.ajax({
            type: 'DELETE', //metoodo 
            url: 'tipo_eventos/' + id, //id del delete
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function () {

                setTimeout("location.href='tipo_eventos'", 1000);

            }
        });

    }
}

function evento_guardar() {

    $.ajax({
        type: 'POST', //metoodo
        url: 'eventos', //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
            'name': $('#name').val(),
            'tipo': $('#tipo_evento_id').val(),
            'inicio': $('#fec_ini').val(),
            'final': $('#fec_fin').val(),
            'cupos': $('#cupos').val(),
            'periocidad': $('#periocidad_id').val(),
            'observacion': $('#observacion').val(),

        },
        success: function (data) {

            if (data.status === 500) {

                if (data.errors['name']) {

                    $('#name').attr('style', 'border-color:red');
                    $('#name_error').html(data.errors['name']);
                    $('#name_error').removeAttr('style');

                }
                if (data.errors['tipo']) {

                    $('#tipo_evento_id').attr('style', 'border-color:red');
                    $('#tipo_error').html(data.errors['tipo']);
                    $('#tipo_error').removeAttr('style');

                }
                if (data.errors['inicio']) {

                    $('#fec_ini').attr('style', 'border-color:red');
                    $('#ini_error').html(data.errors['inicio']);
                    $('#ini_error').removeAttr('style');

                }
                if (data.errors['final']) {

                    $('#fec_fin').attr('style', 'border-color:red');
                    $('#fin_error').html(data.errors['final']);
                    $('#fin_error').removeAttr('style');

                }
                if (data.errors['cupos']) {

                    $('#cupos').attr('style', 'border-color:red');
                    $('#cupos_error').html(data.errors['cupos']);
                    $('#cupos_error').removeAttr('style');

                }
                if (data.errors['periocidad']) {

                    $('#periocidad_id').attr('style', 'border-color:red');
                    $('#per_error').html(data.errors['periocidad']);
                    $('#per_error').removeAttr('style');

                }

                setTimeout(function () {
                    $('#name_error').fadeOut(1500);
                    $('#name').removeAttr('style');

                    $('#tipo_error').fadeOut(1500);
                    $('#tipo_evento_id').removeAttr('style');

                    $('#ini_error').fadeOut(1500);
                    $('#fec_ini').removeAttr('style');

                    $('#fin_error').fadeOut(1500);
                    $('#fec_fin').removeAttr('style');

                    $('#cupos_error').fadeOut(1500);
                    $('#cupos').removeAttr('style');

                    $('#per_error').fadeOut(1500);
                    $('#periocidad_id').removeAttr('style');
                }, 6000);

            } else {

                $('#evento_crear').modal('hide');
                setTimeout("location.href='eventos'", 500);

            }
        }
    });



}

function evento_eliminar(id) {

    $.ajax({
        type: 'DELETE', //metoodo 
        url: 'eventos/' + id, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function () {

            setTimeout("location.href='eventos'", 1000);

        }
    });

}

function validar_rut() {
    
    //Obtenemos el rut
    rut = $('#rut').val();
    $.ajax({
        type: 'POST', //metoodo 
        url: '/validar/'+rut, //id del delete
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {
            
            //Si el rut es valido se habilita el registro
            if(data==1){
                 
                alertify.success('<strong style="color:white;">RUT Valido</strong>');
                $('#rut').attr('style', 'border-color:green');

            }else{
                 
                alertify.error('<strong style="color:white;">RUT Invalido, verifique...</strong>');
                $('#rut').val('');
            }
           

        }
    });
    
}
