<?php
use App\Http\Controllers\FamiliasController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return view('auth.login');
});

/*Calles*/
Route::resource('calles', 'CallesController')->middleware('auth');
Route::post('calles/cambiar_status/{id}', 'CallesController@cambiar_status');

/*Comunas*/
Route::resource('comunas', 'ComunasController')->middleware('auth');
Route::post('comunas/cambiar_status/{id}', 'ComunasController@cambiar_status');

/*Estados Civiles*/
Route::resource('estados_civiles', 'EstadosCivilsController')->middleware('auth');
Route::post('estados_civiles/cambiar_status/{id}', 'EstadosCivilsController@cambiar_status');

/*Etarios*/
Route::resource('etarios', 'EtariosController')->middleware('auth');
Route::post('etarios/cambiar_status/{id}', 'EtariosController@cambiar_status');

/*Etnias*/
Route::resource('etnias', 'EtniasController')->middleware('auth');
Route::post('etnias/cambiar_status/{id}', 'EtniasController@cambiar_status');

/*Inscripciones*/
//Route::resource('inscripciones', 'InscripcionesController')->middleware('auth');

/*Nacionalidades*/
Route::resource('nacionalidades', 'NacionalidadesController')->middleware('auth');
Route::post('nacionalidades/cambiar_status/{id}', 'NacionalidadesController@cambiar_status');

/*Parentescos*/
Route::resource('parentescos', 'ParentescosController')->middleware('auth');
Route::post('parentescos/cambiar_status/{id}', 'ParentescosController@cambiar_status');

/*Periocidades*/
Route::resource('periocidades', 'PeriocidadesController')->middleware('auth');
Route::post('periocidades/cambiar_status/{id}', 'PeriocidadesController@cambiar_status');

/*Poblaciones*/
Route::resource('poblaciones', 'PoblacionesController')->middleware('auth');
Route::post('poblaciones/cambiar_status/{id}', 'PoblacionesController@cambiar_status');

/*Regiones*/
Route::resource('regiones', 'RegionesController')->middleware('auth');
Route::post('regiones/cambiar_status/{id}', 'RegionesController@cambiar_status');

/*Roles*/
//Route::resource('roles', 'RolesController')->middleware('auth');

/*Tipo Eventos*/
Route::resource('tipo_eventos', 'TipoEventosController')->middleware('auth');
Route::post('tipo_eventos/cambiar_status/{id}', 'TipoEventosController@cambiar_status');

/*Eventos*/
Route::resource('eventos', 'EventosController')->middleware('auth');
Route::get('eventos-listado','EventosController@listado')->middleware('auth')->name('eventos.listado');

/*Unidades*/
Route::resource('unidades', 'UnidadesController')->middleware('auth');
Route::post('unidades/cambiar_status/{id}', 'UnidadesController@cambiar_status');

/*Integrantes*/
Route::resource('integrantes', 'IntegrantesController')->middleware('auth');
Route::post('/validar/{rut}', 'IntegrantesController@validar_rut');
Route::post('integrantes/cambiar_status/{id}', 'IntegrantesController@cambiar_status');
Route::get('perfil', function () {
    return view('miembros.miembro');
});


/*Familias*/
Route::resource('familias', 'FamiliasController')->middleware('auth');
Route::delete('familias/{id}', 'FamiliasController@destroy')->middleware('auth');
Route::post('familias/ver/{id}', 'FamiliasController@ver')->middleware('auth');
Route::post('familias/cargar/{id}', 'FamiliasController@cargar')->middleware('auth');
Route::post('familias/salir/{id}', 'FamiliasController@salir')->middleware('auth');
Route::post('familias/anadir/{id}/{fa}', 'FamiliasController@anadir')->middleware('auth');
Route::get('familia', 'FamiliasController@familia_user')->middleware('auth')->name('familia.user');

/*Empresas*/
Route::resource('empresas', 'EmpresasController')->middleware('auth');

/*Productos*/
Route::resource('productos', 'ProductosController')->middleware('auth');

