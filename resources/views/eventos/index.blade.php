@extends('layouts.admin')
@section('content')

<!--Ubicacion-->
<section class="content-header">
      <h1>
        Gestion de Eventos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Tarjeta Vecinos</a></li>
        <li><a href="#">Eventos</a></li>

      </ol>
      <!--Alerta de Eventos-->           
</section>
            
<!--Seccion para el contenido-->
<section class="content" id="content">
                  @if (session('message'))
                     <div class="alert alert-{{ session('class') }} mt-3">

                     <h4><i class="icon fa fa-check"></i>{{ session('message')}}</h4>

                     </div>       
                  @endif
                  <div class="row">
                        <div class="col-md-12">
                                    <div class="box box-success">
                                      <div class="box-header with-border">
                                        <h3 class="box-title">Listado de Eventos</h3>
                                        <button class="btn btn-primary" style="float: right;" data-target='#evento_crear' data-toggle='modal'>Registro de Evento</button>
                                      </div>
                                      <!-- /.box-header -->
                                      <div class="box-body">
                                        <table class="table table-bordered" id="lista">
                                          <thead>
                                                <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Evento</th>
                                                <th>Tipo de Eventos</th>
                                                <th>Cupos</th>
                                                <th>Fecha de Inicio</th>
                                                <th>Fecha de Final</th>
                                                <th>Acciones</th>
                                                </tr>
                                          </thead>
                                           <tbody>
                                             @foreach ($eventos as $event)
                                             <tr>
                                              <td>{{ $event->id}}</td>
                                              <td>{{ $event->name }}</td>
                                              <td>{{ $event->tipo_evento->name }}</td>
                                              <td><span class="label label-info">{{ $event->cupos }}</span></td>
                                              <td>{{ $event->fec_ini }}</td>
                                              <td>{{ $event->fec_fin}}</td>
                                              <td> <button class="btn btn-danger" onclick="evento_eliminar('{{ $event->id }}')"><i class="fa fa-trash"></i></button></td>
                                            </tr>    
                                             @endforeach
                                           </tbody>
                                      </table>
                                      </div>
                                      <!-- /.box-body -->
                                    </div>
                            <!-- /.box -->
                  </div>
</div>

<div class="modal fade" id="evento_crear">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Registrar Evento</h4>
              </div>
              <div class="modal-body">
                    <form action="{{ route('eventos.store') }}" method="POST">

                                  @csrf

                              <div class="box-body">
                              <div class="form-group">
                                      <label for="exampleInputEmail1">Nombre del Evento:</label>
                                      <input type="text" name="name" id="name" class="form-control" placeholder="Ej: Domingo Familiar, ..." required="required" min="5">
                                      <span id="name_error" class="label label-danger" style="display:none"></span>
                              </div>
                              <div class="form-group">
                                      <label for="exampleInputPassword1">Tipo de Evento</label>
                                      <select class="form-control" name="tipo_evento_id" id="tipo_evento_id" required="required">
                                        <optgroup>
                                          <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                          @foreach ($tipo as $t)
                                          <option value="{{ $t->id }}">{{ $t->name }}</option>    
                                          @endforeach
                                        </optgroup>
                                      </select>
                                      <span id="tipo_error" class="label label-danger" style="display:none"></span>
                               </div>
                               <div class="row">
                               <div class="form-group col-md-6">
                                      <label for="exampleInputEmail1">Fecha de Inicio:</label>
                                      <input type="date" class="form-control" name="fec_ini" id="fec_ini" required="required">
                                      <span id="ini_error" class="label label-danger" style="display:none"></span>
                               </div>
                               <div class="form-group col-md-6">
                                      <label for="exampleInputEmail1">Fecha de Finalizacion:</label>
                                      <input type="date" class="form-control" name="fec_fin" id="fec_fin" required="required">
                                      <span id="fin_error" class="label label-danger" style="display:none"></span>
                               </div>
                               </div>
                               <div class="row">
                               <div class="form-group col-md-6">
                                      <label for="exampleInputEmail1">Cupos Disponibles:</label>
                                      <input type="number" min="1" class="form-control" name="cupos" id="cupos" required="required">
                                      <span id="cupos_error" class="label label-danger" style="display:none"></span>
                               </div>
                               <div class="form-group col-md-6">
                                      <label for="exampleInputEmail1">Periocidad:</label>
                                      <select class="form-control" name="periocidad_id" id="periocidad_id" required="required">
                                        <optgroup>
                                          <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                          @foreach ($periodos as $p)
                                          <option value="{{ $p->id }}">{{ $p->name }}</option>    
                                          @endforeach
                                        </optgroup>
                                      </select>
                                      <span id="per_error" class="label label-danger" style="display:none"></span>
                                </div>
                               </div>
                               <div class="form-group">
                                      <label for="exampleInputEmail1">Observacion:</label>
                                      <textarea class="form-control" name="observacion" id="observacion" required="required"></textarea>
                                    </div>
                               </div>
                                  <!-- /.box-body -->
              </div>
              <div class="modal-footer">
                  <div class="col-md-12 text-center">
                  <button type="button" onclick="evento_guardar()" class="btn btn-success">Guardar Evento</button>
                  </div>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>
</section>

@endsection