@extends('layouts.admin')
@section('content')

@if(auth::user()->rol_id==0)
{{-- Content-Header --}}
<section class="content-header">
      <h1>
        Panel de Control
        <small>Control general</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Principal</a></li>
        <li class="active">Panel de Control</li>
      </ol>
</section>

{{-- Content --}}
<section class="content">
  <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{ $users }}</h3>

              <p>Usuarios Registrados</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="{{ route('integrantes.index') }}" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ $empresas }}</h3>

              <p>Empresas</p>
            </div>
            <div class="icon">
              <i class="fa fa-building-o"></i>
            </div>
            <a href="{{ route('empresas.index') }}" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>0<sup style="font-size: 20px"></sup></h3>

              <p>Tarjetas</p>
            </div>
            <div class="icon">
              <i class="fa fa-credit-card"></i>
            </div>
            <a href="#" title="En Construcción" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ $events }}</h3>

              <p>Eventos</p>
            </div>
            <div class="icon">
              <i class="fa fa-calendar-check-o"></i>
            </div>
            <a href="{{ route('eventos.index') }}" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
  </div>

{{--Lista de Eventos--}}
<div class="box box-primary">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">Proximos Eventos</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
              <ul class="todo-list ui-sortable">
                @foreach ($eventos as $e)
                <li>
                  <!-- drag handle -->
                  <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <!-- checkbox -->
                  <!-- todo text -->
                  <span class="text">{{ $e->name  }}</span>
                  <!-- Emphasis label -->
                    <small class="label label-info"><i class="fa fa-clock-o"></i> {{ $e->created_at }}</small>
                  <!-- General tools such as edit or delete-->
                  <div class="tools">
                    {{--<i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>--}}
                  </div>
                </li>
                @endforeach
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
            </div>
</div>
</section>
@else

{{-- Content-Header --}}
<section class="content-header">
    <h1>
      Bienvenido
      <small>Panel principal</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Principal</a></li>
      <li class="active">Panel principal</li>
    </ol>
</section>

{{-- Content --}}
<section class="content">
<div class="row">      
       <div class="col-lg-6 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3>5</h3>

            <p>Solicitudes</p>
          </div>
          <div class="icon">
            <i class="fa fa-file-text"></i>
          </div>
          <a href="{{ route('empresas.index') }}" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-6 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-orange">
          <div class="inner">
            <h3>4</h3>
            <p>Pagos</p>
          </div>
          <div class="icon">
            <i class="fa fa-credit-card"></i>
          </div>
          <a href="{{ route('integrantes.index') }}" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->

</div>

<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Notificaciones</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <ul class="products-list product-list-in-box">
        <li class="item">
          <div class="product-img">
            <i class="fa fa-check-square fa-2x "></i>
          </div>
          <div class="product-info">
            <a href="javascript:void(0)" class="product-title"> Solicitud de Tarjeta
              <span class="label label-warning pull-right">2019-04-29</span></a>
            <span class="product-description">
                  Procesada
                </span>
          </div>
        </li>
        <!-- /.item -->
        <li class="item">
            <div class="product-img">
              <i class="fa fa-check-square fa-2x "></i>
            </div>
            <div class="product-info">
              <a href="javascript:void(0)" class="product-title"> Aprobacion de Pago
                <span class="label label-warning pull-right">2019-04-28</span></a>
              <span class="product-description">
                  <span class="label label-success">1000,00 $</span>  
                  </span>
            </div>
          </li>
        <!-- /.item -->
      </ul>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
      <a href="javascript:void(0)" class="uppercase">Ver todas</a>
    </div>
    <!-- /.box-footer -->
  </div>
</section>
@endif
@endsection
