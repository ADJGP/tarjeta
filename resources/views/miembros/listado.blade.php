@extends('layouts.admin')

@section('content')
{{-- Content-Header --}}
<section class="content-header">
    <h1>
      Bienvenido
      <small>Eventos</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Principal</a></li>
      <li class="active">Eventos</li>
    </ol>
</section>

{{-- Content --}}
<section class="content">
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Eventos Programados</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered" id="lista">
              <thead>
                    <tr>
                    <th style="width: 10px">#</th>
                    <th>Evento</th>
                    <th>Tipo de Eventos</th>
                    <th>Fecha de Inicio</th>
                    <th>Fecha de Final</th>
                    </tr>
              </thead>
               <tbody>
                 @foreach ($eventos as $event)
                 <tr>
                  <td>{{ $event->id}}</td>
                  <td>{{ $event->name }}</td>
                  <td>{{ $event->tipo_evento->name }}</td>
                  <td><span class="label label-success">{{ $event->fec_ini }}</span></td>
                  <td><span class="label label-danger">{{ $event->fec_fin}}</span></td>
                </tr>    
                 @endforeach
               </tbody>
          </table>
          </div>
          <!-- /.box-body -->
        </div>
<!-- /.box -->
</section>   
@endsection
