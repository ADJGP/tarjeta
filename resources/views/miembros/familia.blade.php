@extends('layouts.admin')

@section('content')
{{-- Content-Header --}}
<section class="content-header">
    <h1>
      Bienvenido
      <small>familia</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Principal</a></li>
      <li class="active">Familia</li>
    </ol>
</section>

{{-- Content --}}
<section class="content">

@if ($family === 0)
    <div class="box box-solid">
                  <div class="box-header with-border bg-aqua">
                    <i class="fa fa-exclamation"></i>
      
                    <h3 class="box-title">Aun no estas agregado</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <blockquote>
                      <p>Al parecer no te has incorporado a una familia, utiliza nuestro buscador para encontrar tu grupo familiar y agregate!</small>
                    </blockquote>
                  </div>
                  <!-- /.box-body -->
    </div>
    <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Familias Inscritas</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-bordered" id="lista">
            <thead>
                  <tr>
                  <th>Codigo Familia</th>
                  <th>Apellidos</th>
                  <th>Direccion</th>
                  <th>Acciones</th>
                  </tr>
            </thead>
             <tbody>
               @foreach ($familias as $familia)
                <tr>
                <td class="text-center"><span class="label label-primary">{{ $familia->cod_familia }}</span></td>
                <td>{{ $familia->apellidos }}</td>
                <td>{{ $familia->direccion }}</td>
                <td class="text-center"><button class="btn btn-info">Vincular</button></td>
                </tr>   
               @endforeach
             </tbody>
        </table>
        </div>
        <!-- /.box-body -->
      </div>
<!-- /.box -->
@else
    <h4>Si esta en una familia {{ $result->apellidos }}</h4>
@endif
                
    
</section>   
@endsection
