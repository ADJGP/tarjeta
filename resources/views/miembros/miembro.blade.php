@extends('layouts.admin')

@section('content')
{{-- Content-Header --}}
<section class="content-header">
    <h1>
      Bienvenido
      <small>Informacion general de la cuenta</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Principal</a></li>
      <li class="active">Cuenta</li>
    </ol>
</section>

{{-- Content --}}
<section class="content">

        <div class="row">
          <div class="col-md-3">
  
            <!-- Profile Image -->
            <div class="box box-primary">
              <div class="box-body box-profile">
  
                <h5 class="profile-username text-center">{{ Auth::user()->integrante->nombres.' '.Auth::user()->integrante->apellidos }}</h5>
  
                <p class="text-muted text-center">Miembro</p>
  
                <ul class="list-group list-group-unbordered">
                  <li class="list-group-item">
                    <b>RUT</b> <a class="pull-right">{{ Auth::user()->integrante->rut }}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Email</b> <a class="pull-right">{{ Auth::user()->email }}</a>
                  </li>
                </ul>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#settings" data-toggle="tab" aria-expanded="false">Actualizar</a></li>
              </ul>
              <div class="tab-content">
  
                <div class="tab-pane active" id="settings">
                  <form class="form-horizontal">
                      <div class="form-group{{ $errors->has('fecha_nacimiento') ? 'has-error' : '' }} col-md-6">
                          <label>Fecha de Nacimiento:</label>
                         <input type="text" id="datemask2" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask="" placeholder="dd/mm/aaaa" class="form-control" name="fecha_nacimiento" value="{{ old('fecha_nacimiento') }}" required="required">
                         <span class="help-block label label-danger">{{ $errors->first('fecha_nacimiento') }}</span>
                   </div>
                   <div class="form-group{{ $errors->has('sexo') ? 'has-error' : '' }} col-md-6">
                      <label>Sexo:</label>
                      <select class="form-control" name="sexo">
                        <option selected="selected" disabled="disabled">-- Seleccione --</option>
                        <optgroup>
                               <option value="M" {{ old('sexo') == 'M' ? 'selected' : '' }}>Masculino</option>
                               <option value="F" {{ old('sexo') == 'F' ? 'selected' : '' }}>Femenino</option>
                        </optgroup>
                      </select>
                      <span class="help-block label label-danger">{{ $errors->first('sexo') }}</span>
                </div>
                <div class="form-group{{ $errors->has('estado_civil_id') ? 'has-error' : '' }} col-md-6">
                    <label>Estado Civil:</label>
                    <select class="form-control" name="estado_civil_id">
                      <option selected="selected" disabled="disabled">-- Seleccione --</option>
                      <optgroup>
                            
                      </optgroup>
                    </select>
                    <span class="help-block label label-danger">{{ $errors->first('estado_civil_id') }}</span>
              </div>
              <div class="form-group{{ $errors->has('telf_movil') ? 'has-error' : '' }} col-md-6">
                  <label>Telefono Movil:</label>
                  <input type="text" class="form-control" name="telf_movil" data-inputmask="'mask': ['9 9999-9999', '+(99) 9 9999-9999']" data-mask="" value="{{ old('telf_movil') }}" required="required">
                  <span class="help-block label label-danger">{{ $errors->first('telf_movil') }}</span>
            </div>
            <div class="form-group{{ $errors->has('telf_fijo') ? 'has-error' : '' }} col-md-6">
                <label>Telefono Fijo:</label>
               <input type="text" class="form-control" name="telf_fijo" value="{{ old('telf_fijo') }}" data-inputmask="'mask': ['9 9999-9999', '+(99) 9 9999-9999']" data-mask="">
               <span class="help-block label label-danger">{{ $errors->first('telf_fijo') }}</span>
            </div>
            <div class="form-group{{ $errors->has('etnia_id') ? 'has-error' : '' }} col-md-6">
                <label>Etnia:</label>
                <select class="form-control" name="etnia_id">
                  <option selected="selected" disabled="disabled">-- Seleccione --</option>
                  <optgroup>
                         
                  </optgroup>
                </select>
                <span class="help-block label label-danger">{{ $errors->first('etnia_id') }}</span>
          </div>
          <div class="form-group{{ $errors->has('etario_id') ? 'has-error' : '' }} col-md-6">
              <label>Etario:</label>
              <select class="form-control" name="etario_id">
                <option selected="selected" disabled="disabled">-- Seleccione --</option>
                <optgroup>
                      
                </optgroup>
              </select>
              <span class="help-block label label-danger">{{ $errors->first('etario_id') }}</span>
        </div>
        <div class="form-group{{ $errors->has('nacionalidad_id') ? 'has-error' : '' }} col-md-6">
            <label>Nacionalidad:</label>
            <select class="form-control" name="nacionalidad_id">
              <option selected="selected" disabled="disabled">-- Seleccione --</option>
              <optgroup>
                     
              </optgroup>
            </select>
            <span class="help-block label label-danger">{{ $errors->first('nacionalidad_id') }}</span>
         </div>
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-danger">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
  
      </section>
@endsection