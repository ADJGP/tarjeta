@extends('layouts.admin')
@section('content')

<!--Ubicacion-->
<section class="content-header">
      <h1>
        Gestion de Periocidades
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Tarjeta Vecinos</a></li>
        <li><a href="#">Periocidades</a></li>

      </ol>
      <!--Alerta de Eventos-->           
</section>
            
<!--Seccion para el contenido-->
<section class="content" id="content">
                  @if (session('message'))
                     <div class="alert alert-{{ session('class') }} mt-3">

                     <h4><i class="icon fa fa-check"></i>{{ session('message')}}</h4>

                     </div>       
                  @endif
                  <div class="row">
                        
                        <div class="col-md-6">
                        
                                <div class="box box-primary">

                                  <div class="box-header with-border">
                                    <h3 class="box-title">Crear Periocidad</h3>
                                  </div>
                                  <!-- /.box-header -->
                                            <!-- form start -->
                                            <form role="form" action="{{ route('periocidades.store') }}" method="POST">
                                            <div class="box-body">

                                                @csrf

                                                  <div class="form-group{{ $errors->has('name') ? 'has-error' : '' }} col-md-12">

                                                        <label for="exampleInputEmail1">Nombre:</label>
                                                        <input type="text" class="form-control" min="4" name="name" value="{{ old('name') }}" placeholder="Nombre..." required="required" autofocus="autofocus">
                                                        <span class="help-block label label-danger">{{ $errors->first('name') }}</span>

                                                  </div>

                                          </div>
                                                    <!-- /.box-body -->

                                                  <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                                    <button type="reset" class="btn btn-default">Cancelar</button>
                                                  </div>
                                            </form>
                                 </div>

                        </div>

                        <div class="col-md-6">
                                    <div class="box box-success">
                                      <div class="box-header with-border">
                                        <h3 class="box-title">Listado de Periocidades</h3>
                                      </div>
                                      <!-- /.box-header -->
                                      <div class="box-body">
                                        <table class="table table-bordered" id="lista">
                                          <thead>
                                                <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Periocidad</th>
                                                <th>Estado</th>
                                                <th style="width: 40px">Acciones</th>
                                                </tr>
                                          </thead>
                                           <tbody>
                                            @foreach($periocidades as $c)
                                             <tr>
                                               <td>{{ $loop->iteration }}</td>
                                               <td>{{ $c->name }}</td>
                                               <td>
                                                @if($c->status==1)
                                                   <span class="label label-primary">Activo</span>
                                                @else
                                                   <span class="label label-default">Desactivado</span>  
                                                @endif 
                                               </td>
                                               <td>
                                                @if($c->status==1)
                                                <button class="btn btn-danger" title="Desactivar" onclick="periodo_status('{{ $c->id }}')">
                                                  <i class="fa fa-pause"></i>
                                                </button>
                                                @else
                                                <button class="btn btn-success" title="Activar" onclick="periodo_status('{{ $c->id }}')">
                                                  <i class="fa fa-play"></i>
                                                </button>
                                               @endif 
                                               </td>
                                             </tr>
                                            @endforeach
                                           </tbody>
                                      </table>
                                      </div>
                                      <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                           </div>

                  </div>
</section>


@endsection