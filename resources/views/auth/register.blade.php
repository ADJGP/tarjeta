@extends('layouts.login')
@section('content')
<div class="register-box box-primary" >

  <div class="register-box-body">
    <center>
    <img src="{{ asset('img/logo.png') }}" style="width: 80%" >
     <br>
     <p class="login-box-msg">Ingresa los datos solicitados</p>
  </center>
    

     
      <div class="form-group has-feedback">
          <form method="POST" action="{{ route('register') }}" novalidate="">
                        @csrf
        
        <div class="form-group has-feedback {{ $errors->has('rol_id') ? 'has-error' : '' }}">
        <select class="form-control" name="rol_id" required="required">
               <optgroup>
                 <option disabled="disabled" selected="selected" value="0">--Seleccione--</option>
                 <option value="1" >Vecino Local</option>
                 <option value="2">Vecino Foraneo</option>
               </optgroup>
        </select>        
        <span class="help-block">{{ $errors->first('rol_id') }}</span>
        </div>

        <div class="form-group has-feedback {{ $errors->has('rut') ? 'has-error' : '' }}">        
        <input type="text" class="form-control " name="rut" placeholder="RUT">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('rut') }}</span>
        </div>

        <div class="form-group has-feedback {{ $errors->has('nombres') ? 'has-error' : '' }}">        
        <input type="text" class="form-control " name="nombres" placeholder="Nombres">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('nombres') }}</span>
        </div>

        <div class="form-group has-feedback {{ $errors->has('apellidos') ? 'has-error' : '' }}">        
        <input type="text" class="form-control " name="apellidos" placeholder="Apellidos">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('apellidos') }}</span>
        </div>

         <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
          <input type="text" class="form-control " name="email" placeholder="Correo Electrónico">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('email') }}</span>
        </div>

      <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }} ">
        <input type="password" class="form-control" name="password" placeholder="Contraseña">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('password') }}</span>
      </div>

      <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}" >
        <input type="password" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
      </div>

    <div class="row">
       
        <!-- /.col -->
        <div class="col-xs-12">
          <center>
          <button type="submit" class="btn btn-success " ><span class="glyphicon glyphicon-log-in "></span> Crear Usuario</button>
          <a href="{{ route('login') }}" class="btn btn-default "><span class="glyphicon glyphicon-arrow-left"></span> Volver</a>
       

         <br>
                               </center>
        </div>
        <!-- /.col -->
      </div>



                        
          </form>

        </div>
   

 

    


  </div>
 
</div>

@endsection
