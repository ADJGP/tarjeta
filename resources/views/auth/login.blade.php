@extends('layouts.login') 
@section('content')
<div class="login-box">

    <!-- /.login-logo -->
    <div class="login-box-body">
       
        <div class="login-logo">
            {{--<a href="../../index2.html"><b>CASTRO</b>municipio</a>--}}
            <img src="{{ asset('img/logo.png') }}" style="width: 80%">
        </div> 
        <p class="login-box-msg">Ingresa tus credenciales, para entrar</p>
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                <input type="email" name="email" class="form-control" placeholder="Correo electronico">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                <span class="help-block">{{ $errors->first('email') }}</span>
            </div>
            <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                <input type="password" name="password" class="form-control" placeholder="Contraseña">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <span class="help-block">{{ $errors->first('password') }}</span>
            </div>
            <div class="row">
                <!-- /.col -->
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Acceder</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <div class="social-auth-links text-center">
            <p>- || -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> 
              Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i>
              Google+</a>
              @if (Route::has('password.request'))
              <a href="{{ route('password.request') }}">has olvidado tu contraseña?</a><br>
          @endif
             
              <a href="{{ route('register') }}" class="text-center">Registrar nuevo usuario</a>
        </div>
        <!-- /.social-auth-links -->

        

    </div>
    <!-- /.login-box-body -->
</div>
@endsection