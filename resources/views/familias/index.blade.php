@extends('layouts.admin')
@section('content')

<!--Ubicacion-->
<section class="content-header">
      <h1>
        Gestion de Familias
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Tarjeta Vecinos</a></li>
        <li><a href="#">Familias</a></li>

      </ol>
      <!--Alerta de Eventos-->           
</section>
            
<!--Seccion para el contenido-->
<section class="content" id="content">
                  @if (session('message'))
                     <div class="alert alert-{{ session('class') }} mt-3">

                     <h4><i class="icon fa fa-check"></i>{{ session('message')}}</h4>

                     </div>       
                  @endif
                  <div class="row">
                    <div class="form-group col-md-12">
                                <div class="alert alert-info">
                                       <p><strong class="text-uppercase">Bienvenido a el gestor de familias!</strong><br> A continuación  para crear una familia, haga click en el boton (Crear Familia). </p>
                                        <p>Cada vecino solo podra ser integrante de una familia.</p>
                                </div>
                                                    
                    </div> 

                        <div class="col-md-12">
                                    <div class="box box-success">
                                      <div class="box-header with-border">
                                        <h3 class="box-title">Listado de Familias</h3>
                                        
                                        <form action="{{ route('familias.store') }}" method="POST">
                                          @csrf
                                          <button type="submit" class="btn btn-success" style="float: left; margin-right: 1rem; margin-top: 1rem;">Crear Familia</button>
                                        </form>
                                         <button class="btn btn-primary"  style="float: left; margin-top: 1rem;" onclick="lanzar()" title="Agregar Integrantes">Agregar Integrantes</button> 
                                        
                                      </div>
                                      <!-- /.box-header -->
                                      <div class="box-body">
                                        <table class="table table-bordered" id="lista">
                                          <thead>
                                                <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Codigo</th>
                                                <th>Acciones</th>
                                                </tr>
                                          </thead>
                                           <tbody>
                                            @foreach($familias as $fam)
                                             <tr>
                                                 <td>{{ $loop->iteration }}</td>
                                                 <td>{{ $fam->cod_familia }}</td>
                                                 <td>
                                                     <button class="btn btn-success" onclick="ver_integrantes('{{ $fam->cod_familia }}')" title="Agregar Integrantes"><i class="fa fa-users"></i></button>
                                                     <button class="btn btn-danger" onclick="eliminar_familia('{{ $fam->id }}')" title="Eliminar Familia"><i class="fa fa-close"></i></button>
                                                 </td>
                                             </tr>
                                            @endforeach 
                                           </tbody>
                                      </table>
                                      </div>
                                      <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                           </div>

                  </div>

<div class="modal fade" id="modal-default" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Listado de Integrantes Disponibles</h4>
              </div>
              <div class="modal-body">
                 <div class="form-group col-md-12">

                                        <label>Seleccione la Familia:</label>
                                        <select class="form-control" id="familia" onchange="cargar_integrantes()">
                                          <optgroup>
                                                  <option selected="selected" disabled="disabled" value="0">-- Seleccione --</option>
                                                  @foreach($familias as $fam)
                                                          <option value="{{ $fam->cod_familia }}" >{{ $fam->cod_familia }}</option>
                                                  @endforeach 
                                          </optgroup>
                                        </select>
                </div>
                <div class="form-group col-md-12">
                              <label>Agregue los Integrantes Disponibles:</label>
                                        <table class="table table-striped">
                                          <thead>
                                                <tr>
                                                <th style="width: 10px">#</th>
                                                <th>RUT</th>
                                                <th>Integrante</th>
                                                <th>Acciones</th>
                                                </tr>
                                          </thead>
                                           <tbody id="integrantes">
                                           </tbody>
                  </table>    
                </div>
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-default2">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Integrantes de la Familia: <small id="name-f"></small></h4>
              </div>
              <div class="modal-body">
                      <table class="table table-striped">
                                  <thead>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>RUT</th>
                                            <th>Integrante</th>
                                        </tr>
                                  </thead>
                                  <tbody id="integrantes2">
                                  </tbody>
                      </table> 
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
</div>
</section>
@endsection