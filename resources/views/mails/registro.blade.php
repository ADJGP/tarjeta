Hola <i>{{ $registro->receiver }}</i>, bienvenido a nuestra plataforma. 
<br>
<p>Ya que te has registrado a continuacion te dejamos los datos para acceder a tu cuenta.</p>

<p><u>Datos de Acceso:</u></p>
 
<div>
<p><b>Email:</b>&nbsp;{{ $registro->usuario  }}</p>
<p><b>Clave de Acceso:</b>&nbsp;{{ $registro->clave  }}</p>
</div>

<div>
<p class="text-danger">Recuerda proteger estos datos que te suministramos para garantizar la seguridad de tu cuenta.</p>
</div>


Gracias,
<br/>
<i>{{ $registro->sender }}</i>