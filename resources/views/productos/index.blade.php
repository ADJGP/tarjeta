@extends('layouts.admin')
@section('content')

<!--Ubicacion-->
<section class="content-header">
      <h1>
        Gestion de Productos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Tarjeta Vecinos</a></li>
        <li><a href="#">Productos</a></li>

      </ol>
      <!--Alerta de Eventos-->           
</section>
            
<!--Seccion para el contenido-->
<section class="content" id="content">
                  @if (session('message'))
                     <div class="alert alert-{{ session('class') }} mt-3">

                     <h4><i class="icon fa fa-check"></i>{{ session('message')}}</h4>

                     </div>       
                  @endif
                  <div class="row">
                        
                        <div class="col-md-6">
                        
                                <div class="box box-primary">

                                  <div class="box-header with-border">
                                    <h3 class="box-title">Registrar Producto</h3>
                                  </div>
                                  <div class="form-group col-md-12">
                                     <p class="alert alert-info">A continuacion ingrese los datos del Producto a registrar.</p>                   
                                  </div> 
                                  
                                  <!-- /.box-header -->
                                            <!-- form start -->
                                            <form role="form" action="{{ route('productos.store') }}" method="POST">
                                            <div class="box-body">

                                                @csrf


                                                  <div class="form-group{{ $errors->has('empresa_id') ? 'has-error' : '' }} col-md-12">
                                                        <label>Empresa a la que pertenece el producto:</label>
                                                        <select class="form-control" name="empresa_id" required="required">
                                                          <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                                          <optgroup>
                                                                 @foreach($empresas as $emp)
                                                                   <option value="{{ $emp->id }}" {{ old('empresa_id') == $emp->id ? 'selected' : '' }}>{{ $emp->nombre_fantasia }}</option>
                                                                 @endforeach
                                                          </optgroup>
                                                        </select>
                                                        <span class="help-block label label-danger">{{ $errors->first('empresa_id') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('name') ? 'has-error' : '' }} col-md-6">
                                                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Nombre del Producto" required="required">
                                                        <span class="help-block label label-danger">{{ $errors->first('name') }}</span>
                                                  </div> 
                                                  <div class="form-group{{ $errors->has('abreviado') ? 'has-error' : '' }} col-md-6">
                                                        <input type="text" class="form-control" name="abreviado" value="{{ old('abreviado') }}" placeholder="Abreviacion del Producto" required="required">
                                                        <span class="help-block label label-danger">{{ $errors->first('abreviado') }}</span>
                                                  </div> 
                                                  <div class="form-group{{ $errors->has('gloza') ? 'has-error' : '' }} col-md-12">
                                                        <textarea class="form-control" name="gloza" placeholder="Descripcion del Producto" required="required">{{ old('gloza') }}</textarea>
                                                        <span class="help-block label label-danger">{{ $errors->first('gloza') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('vigente') ? 'has-error' : '' }} col-md-3">
                                                        <label>Vigente:</label>
                                                        <input type="number" class="form-control" min="1" name="vigente" value="{{ old('vigente') }}" placeholder="10" required="required" autofocus="autofocus">
                                                        <span class="help-block label label-danger">{{ $errors->first('vigente') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('stock_inicial') ? 'has-error' : '' }} col-md-3">
                                                        <label>Stock Inicial:</label>
                                                        <input type="number" class="form-control" min="1" name="stock_inicial" value="{{ old('stock_inicial') }}" placeholder="10" required="required" autofocus="autofocus">
                                                        <span class="help-block label label-danger">{{ $errors->first('stock_inicial') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('stock_maximo') ? 'has-error' : '' }} col-md-3">
                                                         <label>Stock Maximo:</label>
                                                        <input type="number" class="form-control" min="1" name="stock_maximo" value="{{ old('stock_maximo') }}" placeholder="10" required="required" autofocus="autofocus">
                                                        <span class="help-block label label-danger">{{ $errors->first('stock_maximo') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('stock_minimo') ? 'has-error' : '' }} col-md-3">
                                                         <label>Stock Minimo:</label>
                                                        <input type="number" class="form-control" min="1" name="stock_minimo" value="{{ old('stock_minimo') }}" placeholder="2" required="required" autofocus="autofocus">
                                                        <span class="help-block label label-danger">{{ $errors->first('stock_minimo') }}</span>
                                                  </div> 
                                                  <div class="form-group{{ $errors->has('fec_termino') ? 'has-error' : '' }} col-md-12">
                                                         <label>Fecha de Termino:</label>
                                                        <input type="date" class="form-control" name="fec_termino" value="{{ old('fec_termino') }}" placeholder="Abreviacion del Producto" required="required">
                                                        <span class="help-block label label-danger">{{ $errors->first('fec_termino') }}</span>
                                                  </div>   
                                          </div>
                                                    <!-- /.box-body -->

                                                  <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                                    <button type="reset" class="btn btn-default">Cancelar</button>
                                                  </div>
                                            </form>
                                 </div>

                        </div>

                        <div class="col-md-6">
                                    <div class="box box-success">
                                      <div class="box-header with-border">
                                        <h3 class="box-title">Listado de Productos</h3>
                                      </div>
                                      <!-- /.box-header -->
                                      <div class="box-body">
                                        <table class="table table-bordered" id="lista">
                                          <thead>
                                                <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Producto</th>
                                                <th>Existencia</th>
                                                <th>Fecha de Registro</th>
                                                <th>Acciones</th>
                                                </tr>
                                          </thead>
                                           <tbody>
                                            @foreach($productos as $pro)
                                            <tr>
                                              <td>{{ $loop->iteration}}</td>
                                              <td>{{ $pro->name }}</td>
                                              <td>{{ $pro->vigente }}</td>
                                              <td>{{ $pro->created_at }}</td>
                                              <td><button class="btn btn-danger"><i class="fa fa-close"></i></button></td>
                                            </tr>
                                            @endforeach
                                           </tbody>
                                      </table>
                                      </div>
                                      <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                           </div>

                  </div>
</section>
@endsection