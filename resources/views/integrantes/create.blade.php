@extends('layouts.admin')
@section('content')

<!--Ubicacion-->
<section class="content-header">
      <h1>
        Gestion de Vecinos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Tarjeta Vecinos</a></li>
        <li><a href="#">Vecinos</a></li>

      </ol>
      <!--Alerta de Eventos-->           
</section>
            
<!--Seccion para el contenido-->
<section class="content" id="content">
                  @if (session('message'))
                     <div class="alert alert-{{ session('class') }} mt-3">

                     <h4><i class="icon fa fa-check"></i>{{ session('message')}}</h4>

                     </div>       
                  @endif
                  <div class="row">
                        
                        <div class="col-md-6">
                        
                                <div class="box box-primary">

                                  <div class="box-header with-border">
                                    <h3 class="box-title">Registrar Vecino</h3><br>
                                    <small class="text-primary">  <strong>  Ingrese los datos del vecino a registrar</strong></small>
                                  </div>
                                  <div class="form-group col-md-12">
                                     <p class="text-primary"></p>                   
                                  </div> 
                                  
                                  <!-- /.box-header -->
                                            <!-- form start -->
                                            <form role="form" action="{{ route('integrantes.store') }}" method="POST">
                                            <div class="box-body">

                                                @csrf
                                                   
                                                  <div class="form-group{{ $errors->has('rut') ? 'has-error' : '' }} col-md-12">
                                                        <input type="text" class="form-control" name="rut" id="rut" value="{{ old('rut') }}" placeholder="Ingresar RUT" required="required" onchange='validar_rut()' data-inputmask="'mask': ['9.999.999-9', '99.999.999-9', '9.999.999-k', '99.999.999-k']" data-mask="">
                                                        <span class="help-block label label-danger">{{ $errors->first('rut') }}</span>
                                                  </div> 
                                                  <div class="form-group{{ $errors->has('nombres') ? 'has-error' : '' }} col-md-6">
                                                        <input type="text" class="form-control" min="3" name="nombres" onkeypress="return soloLetras(event)" value="{{ old('nombres') }}" placeholder="Ingresar nombres" required="required"  autocomplete="off">
                                                        <span class="help-block label label-danger">{{ $errors->first('nombres') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('apellidos') ? 'has-error' : '' }} col-md-6">
                                                        <input type="text" class="form-control" min="3" name="apellidos" value="{{ old('apellidos') }}" onkeypress="return soloLetras(event)" placeholder="Ingresar apellidos" required="required"  autocomplete="off">
                                                        <span class="help-block label label-danger">{{ $errors->first('apellidos') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('fecha_nacimiento') ? 'has-error' : '' }} col-md-6">
                                                         <label>Fecha de Nacimiento:</label>
                                                        <input type="text" id="datemask2" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask="" placeholder="dd/mm/aaaa" class="form-control" name="fecha_nacimiento" value="{{ old('fecha_nacimiento') }}" required="required">
                                                        <span class="help-block label label-danger">{{ $errors->first('fecha_nacimiento') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('sexo') ? 'has-error' : '' }} col-md-6">
                                                        <label>Sexo:</label>
                                                        <select class="form-control" name="sexo">
                                                          <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                                          <optgroup>
                                                                 <option value="M" {{ old('sexo') == 'M' ? 'selected' : '' }}>Masculino</option>
                                                                 <option value="F" {{ old('sexo') == 'F' ? 'selected' : '' }}>Femenino</option>
                                                          </optgroup>
                                                        </select>
                                                        <span class="help-block label label-danger">{{ $errors->first('sexo') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('estado_civil_id') ? 'has-error' : '' }} col-md-6">
                                                        <label>Estado Civil:</label>
                                                        <select class="form-control" name="estado_civil_id">
                                                          <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                                          <optgroup>
                                                                 @foreach($estadociviles as $ec)
                                                                 <option value="{{ $ec->id }}" {{ old('estado_civil_id') == $ec->id ? 'selected' : '' }}>{{ $ec->name }}</option>
                                                                 @endforeach
                                                          </optgroup>
                                                        </select>
                                                        <span class="help-block label label-danger">{{ $errors->first('estado_civil_id') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('telf_movil') ? 'has-error' : '' }} col-md-6">
                                                        <label>Telefono Movil:</label>
                                                        <input type="text" class="form-control" name="telf_movil" data-inputmask="'mask': ['9 9999-9999', '+(99) 9 9999-9999']" data-mask="" value="{{ old('telf_movil') }}" required="required">
                                                        <span class="help-block label label-danger">{{ $errors->first('telf_movil') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('telf_fijo') ? 'has-error' : '' }} col-md-6">
                                                         <label>Telefono Fijo:</label>
                                                        <input type="text" class="form-control" name="telf_fijo" value="{{ old('telf_fijo') }}" data-inputmask="'mask': ['9 9999-9999', '+(99) 9 9999-9999']" data-mask="">
                                                        <span class="help-block label label-danger">{{ $errors->first('telf_fijo') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('etnia_id') ? 'has-error' : '' }} col-md-6">
                                                        <label>Etnia:</label>
                                                        <select class="form-control" name="etnia_id">
                                                          <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                                          <optgroup>
                                                                 @foreach($etnias as $et)
                                                                 <option value="{{ $et->id }}" {{ old('estado_civil_id') == $et->id ? 'selected' : '' }}>{{ $et->name }}</option>
                                                                 @endforeach
                                                          </optgroup>
                                                        </select>
                                                        <span class="help-block label label-danger">{{ $errors->first('etnia_id') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('etario_id') ? 'has-error' : '' }} col-md-6">
                                                        <label>Etario:</label>
                                                        <select class="form-control" name="etario_id">
                                                          <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                                          <optgroup>
                                                                 @foreach($etarios as $eta)
                                                                 <option value="{{ $eta->id }}" {{ old('etario_id') == $eta->id ? 'selected' : '' }}>{{ $eta->name }}</option>
                                                                 @endforeach
                                                          </optgroup>
                                                        </select>
                                                        <span class="help-block label label-danger">{{ $errors->first('etario_id') }}</span>
                                                  </div>
                                                   <div class="form-group{{ $errors->has('nacionalidad_id') ? 'has-error' : '' }} col-md-6">
                                                        <label>Nacionalidad:</label>
                                                        <select class="form-control" name="nacionalidad_id">
                                                          <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                                          <optgroup>
                                                                 @foreach($nacionalidades as $n)
                                                                 <option value="{{ $n->id }}" {{ old('nacionalidad_id') == $n->id ? 'selected' : '' }}>{{ $n->name }}</option>
                                                                 @endforeach
                                                          </optgroup>
                                                        </select>
                                                        <span class="help-block label label-danger">{{ $errors->first('nacionalidad_id') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('email') ? 'has-error' : '' }} col-md-12">
                                                        <label>Ingrese su Correo electronico:</label>
                                                        <br>
                                                        <small class="success label label-success mb-5">Su Usuario de Acceso sera enviado a esta direccion</small><br>
                                                        <br>
                                                        <input type="email" class="form-control"  autocomplete="off" name="email"  required="required" value="{{ old('email') }}">
                                                        <span class="help-block label label-danger">{{ $errors->first('email') }}</span>
                                                  </div>

                                                  <div class="form-group has-feedback {{ $errors->has('rol_id') ? 'has-error' : '' }} col-md-12">
                                                      <label>Rol de Usuario:</label>
                                                      <select class="form-control" name="rol_id" required="required">
                                                             <optgroup>
                                                               <option disabled="disabled" selected="selected" value="0">--Seleccione--</option>
                                                               <option value="1" >Vecino Local</option>
                                                               <option value="2">Vecino Foraneo</option>
                                                             </optgroup>
                                                      </select>        
                                                      <span class="help-block label label-danger">{{ $errors->first('rol_id') }}</span>
                                                  </div>
                                                   <div class="form-group{{ $errors->has('inscripcion') ? 'has-error' : '' }} col-md-6">
                                                        <input type="hidden" class="form-control" name="inscripcion"  required="required" value="Ofina de Municipalidad">
                                                        <span class="help-block label label-danger">{{ $errors->first('inscripcion') }}</span>
                                                  </div>


                                          </div>
                                                    <!-- /.box-body -->

                                                  <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                                    <button type="reset" class="btn btn-default">Cancelar</button>
                                                  </div>
                                            </form>
                                 </div>

                        </div>

                        <div class="col-md-6">
                                    <div class="box box-success">
                                      <div class="box-header with-border">
                                        <h3 class="box-title">Listado de Vecinos</h3>
                                      </div>
                                      <!-- /.box-header -->
                                      <div class="box-body">
                                        <table class="table table-bordered" id="lista">
                                          <thead>
                                                <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Vecino</th>
                                                <th>Fecha de Registro</th>
                                                <th>status</th>
                                                </tr>
                                          </thead>
                                           <tbody>
                                            @foreach($integrantes as $c)
                                             <tr>
                                               <td>{{ $loop->iteration }}</td>
                                               <td>{{ $c->nombres }}</td>
                                               <td>{{ $c->created_at }}</td>
                                               <td>
                                                @if($c->status==1)
                                                   <span class="label label-primary">Activo</span>
                                                @else
                                                   <span class="label label-default">Desactivado</span>  
                                                @endif 
                                               </td>
                                             </tr>
                                            @endforeach
                                           </tbody>
                                      </table>
                                      </div>
                                      <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                           </div>

                  </div>
</section>
@endsection