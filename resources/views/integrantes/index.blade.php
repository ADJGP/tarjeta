@extends('layouts.admin')
@section('content')

<!--Ubicacion-->
<section class="content-header">
      <h1>
        Gestion de Vecinos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Tarjeta Vecinos</a></li>
        <li><a href="#">Listado General de Vecinos</a></li>

      </ol>
      <!--Alerta de Eventos-->           
</section>
            
<!--Seccion para el contenido-->
<section class="content" id="content">
                  @if (session('message'))
                     <div class="alert alert-{{ session('class') }} mt-3">

                     <h4><i class="icon fa fa-check"></i>{{ session('message')}}</h4>

                     </div>       
                  @endif
                  <div class="row">

                        <div class="col-md-12">
                                    <div class="box box-success">
                                      <div class="box-header with-border">
                                        <h3 class="box-title">Listado General de Vecinos</h3>
                                      </div>
                                      <!-- /.box-header -->
                                      <div class="box-body">
                                        <table class="table table-bordered" id="lista">
                                          <thead>
                                                <tr>
                                                <th style="width: 10px">#</th>
                                                <th>RUT</th>
                                                <th>Vecino</th>
                                                <th>Fecha de Registro</th>
                                                <th>Status</th>
                                                <th class="text-center">Acciones</th>
                                                </tr>
                                          </thead>
                                           <tbody>
                                            @foreach($integrantes as $c)
                                             <tr>
                                               <td>{{ $loop->iteration }}</td>
                                               <td>{{ $c->rut }}</td>
                                               <td>{{ $c->nombres.' '.$c->apellidos }}</td>
                                               <td>{{ $c->created_at }}</td>
                                               <td>
                                                @if($c->status==1)
                                                   <span class="label label-primary">Activo</span>
                                                @else
                                                   <span class="label label-default">Desactivado</span>  
                                                @endif 
                                               </td>
                                               <td class="text-center">
                                                @if($c->status==1)
                                                <button class="btn btn-warning" title="Desactivar" onclick="integrante_status('{{ $c->id }}')">
                                                  <i class="fa fa-pause"></i>
                                                </button>
                                                @else
                                                <button class="btn btn-success" title="Activar" onclick="integrante_status('{{ $c->id }}')">
                                                  <i class="fa fa-play"></i>
                                                </button>
                                               @endif
                                               <button class="btn btn-primary" title="Modificar" onclick="integrante_edit('{{ $c->id }}')">
                                                  <i class="fa fa-edit"></i>
                                                </button>

                                                <button class="btn btn-danger" title="Eliminar Vecino" onclick="integrante_eliminar('{{ $c->id }}')">
                                                  <i class="fa fa-trash"></i>
                                                </button>  
                                               </td>
                                             </tr>
                                            @endforeach
                                           </tbody>
                                      </table>
                                      </div>
                                      <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                           </div>

                  </div>

{{-- Modal --}}
 <div class="modal fade bs-example-modal-md in" tabindex="-1" role="dialog" aria-hidden="true" id="integrante-edit">
                    <div class="modal-dialog modal-md">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title text-center" id="myModalLabel">Modificacion de Datos Personales</h4>
                        </div>
                        <div class="modal-body">
                         
                        <!-- form start -->
                                            <form role="form"  method="PUT">
                                            <div class="box-body">

                                                @csrf
                                                   
                                                   <div class="form-group{{ $errors->has('rut') ? 'has-error' : '' }} col-md-12">
                                                        <input type="hidden" class="form-control" name="id" id="id">
                                                  </div>
                                                  <div class="form-group{{ $errors->has('rut') ? 'has-error' : '' }} col-md-12">
                                                        <input type="text" class="form-control" name="rut" id="rut" value="{{ old('rut') }}" placeholder="Ingresar RUT" required="required" autofocus="autofocus" min="1">
                                                        <span class="help-block label label-danger">{{ $errors->first('rut') }}</span>
                                                  </div> 
                                                  <div class="form-group{{ $errors->has('nombres') ? 'has-error' : '' }} col-md-6">
                                                        <input type="text" class="form-control" min="4" name="nombres" id="nombres" value="{{ old('nombres') }}" placeholder="Ingresar Nombres" required="required" autofocus="autofocus">
                                                        <span class="help-block label label-danger">{{ $errors->first('nombres') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('apellidos') ? 'has-error' : '' }} col-md-6">
                                                        <input type="text" class="form-control" min="4" name="apellidos" id="apellidos" value="{{ old('apellidos') }}" placeholder="Ingresar Apellidos" required="required" autofocus="autofocus">
                                                        <span class="help-block label label-danger">{{ $errors->first('apellidos') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('fecha_nacimiento') ? 'has-error' : '' }} col-md-6">
                                                         <label>Fecha de Nacimiento:</label>
                                                        <input type="date" class="form-control" name="fecha_nacimiento" id="fecha_nacimiento" value="{{ old('fecha_nacimiento') }}" required="required" autofocus="autofocus">
                                                        <span class="help-block label label-danger">{{ $errors->first('fecha_nacimiento') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('sexo') ? 'has-error' : '' }} col-md-6">
                                                        <label>Sexo:</label>
                                                        <select class="form-control" name="sexo" id="sexo" >
                                                          <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                                          <optgroup>
                                                                 <option value="M" {{ old('sexo') == 'M' ? 'selected' : '' }}>Masculino</option>
                                                                 <option value="F" {{ old('sexo') == 'F' ? 'selected' : '' }}>Femenino</option>
                                                          </optgroup>
                                                        </select>
                                                        <span class="help-block label label-danger">{{ $errors->first('sexo') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('estado_civil_id') ? 'has-error' : '' }} col-md-6">
                                                        <label>Estado Civil:</label>
                                                        <select class="form-control" name="estado_civil_id" id="estado_civil_id">
                                                          <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                                          <optgroup>
                                                                 @foreach($estadociviles as $ec)
                                                                 <option value="{{ $ec->id }}" {{ old('estado_civil_id') == $ec->id ? 'selected' : '' }}>{{ $ec->name }}</option>
                                                                 @endforeach
                                                          </optgroup>
                                                        </select>
                                                        <span class="help-block label label-danger">{{ $errors->first('estado_civil_id') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('telf_movil') ? 'has-error' : '' }} col-md-6">
                                                        <label>Telefono Movil:</label>
                                                        <input type="text" class="form-control" name="telf_movil" id="telf_movil" value="{{ old('telf_movil') }}" required="required" autofocus="autofocus">
                                                        <span class="help-block label label-danger">{{ $errors->first('telf_movil') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('telf_fijo') ? 'has-error' : '' }} col-md-6">
                                                         <label>Telefono Fijo:</label>
                                                        <input type="text" class="form-control" name="telf_fijo" id="telf_fijo" value="{{ old('telf_fijo') }}" required="required" autofocus="autofocus">
                                                        <span class="help-block label label-danger">{{ $errors->first('telf_fijo') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('etnia_id') ? 'has-error' : '' }} col-md-6">
                                                        <label>Etnia:</label>
                                                        <select class="form-control" name="etnia_id" id="etnia_id">
                                                          <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                                          <optgroup>
                                                                 @foreach($etnias as $et)
                                                                 <option value="{{ $et->id }}" {{ old('estado_civil_id') == $et->id ? 'selected' : '' }}>{{ $et->name }}</option>
                                                                 @endforeach
                                                          </optgroup>
                                                        </select>
                                                        <span class="help-block label label-danger">{{ $errors->first('etnia_id') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('etario_id') ? 'has-error' : '' }} col-md-6">
                                                        <label>Etario:</label>
                                                        <select class="form-control" name="etario_id" id="etario_id">
                                                          <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                                          <optgroup>
                                                                 @foreach($etarios as $eta)
                                                                 <option value="{{ $eta->id }}" {{ old('etario_id') == $eta->id ? 'selected' : '' }}>{{ $eta->name }}</option>
                                                                 @endforeach
                                                          </optgroup>
                                                        </select>
                                                        <span class="help-block label label-danger">{{ $errors->first('etario_id') }}</span>
                                                  </div>
                                                   <div class="form-group{{ $errors->has('nacionalidad_id') ? 'has-error' : '' }} col-md-6">
                                                        <label>Nacionalidad:</label>
                                                        <select class="form-control" name="nacionalidad_id" id="nacionalidad_id">
                                                          <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                                          <optgroup>
                                                                 @foreach($nacionalidades as $n)
                                                                 <option value="{{ $n->id }}" {{ old('nacionalidad_id') == $n->id ? 'selected' : '' }}>{{ $n->name }}</option>
                                                                 @endforeach
                                                          </optgroup>
                                                        </select>
                                                        <span class="help-block label label-danger">{{ $errors->first('nacionalidad_id') }}</span>
                                                  </div>
                                          </div>
                                                    <!-- /.box-body -->


                        
                        </div>
                        <div class="modal-footer ">
                          <div class="col-md-12 text-center">
                            <button type="button" onclick="integrante_update()" class="btn btn-success">Guardar Cambios</button>
                          </div>
                          
                          </form>
                        </div>
                      </form>
                      </div>
                    </div>
  </div>
</section>
@endsection