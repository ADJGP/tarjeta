@extends('layouts.admin')
@section('content')

<!--Ubicacion-->
<section class="content-header">
      <h1>
        Gestion de Empresas
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Tarjeta Vecinos</a></li>
        <li><a href="#">Empresas</a></li>

      </ol>
      <!--Alerta de Eventos-->           
</section>
            
<!--Seccion para el contenido-->
<section class="content" id="content">
                  @if (session('message'))
                     <div class="alert alert-{{ session('class') }} mt-3">

                     <h4><i class="icon fa fa-check"></i>{{ session('message')}}</h4>

                     </div>       
                  @endif
                  <div class="row">
                        
                        <div class="col-md-6">
                        
                                <div class="box box-primary">

                                  <div class="box-header with-border">
                                    <h3 class="box-title">Registrar Empresa</h3>
                                  </div>
                                  <div class="form-group col-md-12">
                                     <p class="alert alert-info">A continuacion ingrese los datos de la empresa a registrar.</p>                   
                                  </div> 
                                  
                                  <!-- /.box-header -->
                                            <!-- form start -->
                                            <form role="form" action="{{ route('empresas.store') }}" method="POST">
                                            <div class="box-body">

                                                @csrf
                                                   
                                                  <div class="form-group{{ $errors->has('rut') ? 'has-error' : '' }} col-md-12">
                                                        <input type="number" class="form-control" name="rut" value="{{ old('rut') }}" placeholder="RUT de la Empresa" required="required" autofocus="autofocus" min="1">
                                                        <span class="help-block label label-danger">{{ $errors->first('rut') }}</span>
                                                  </div> 
                                                  <div class="form-group{{ $errors->has('razon_social') ? 'has-error' : '' }} col-md-12">
                                                        <textarea class="form-control" name="razon_social" placeholder="Razon Social de la Empresa" required="required">{{ old('razon_social') }}</textarea>
                                                        <span class="help-block label label-danger">{{ $errors->first('razon_social') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('nombre_fantasia') ? 'has-error' : '' }} col-md-6">
                                                        <input type="text" class="form-control" min="4" name="nombre_fantasia" value="{{ old('nombre_fantasia') }}" placeholder="Nombre de Fantasia" required="required" autofocus="autofocus">
                                                        <span class="help-block label label-danger">{{ $errors->first('nombre_fantasia') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('telefono') ? 'has-error' : '' }} col-md-6">
                                                        <input type="text" class="form-control" min="4" name="telefono" value="{{ old('telefono') }}" placeholder="Numero Telefonico" required="required" autofocus="autofocus">
                                                        <span class="help-block label label-danger">{{ $errors->first('telefono') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('direccion') ? 'has-error' : '' }} col-md-12">
                                                        <textarea class="form-control" name="direccion" placeholder=" Direccion de la Empresa">{{ old('direccion') }}</textarea>
                                                        <span class="help-block label label-danger">{{ $errors->first('direccion') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('contacto') ? 'has-error' : '' }} col-md-6">
                                                        <input type="text" class="form-control" min="4" name="contacto" value="{{ old('contacto') }}" placeholder="Nombre del Contacto">
                                                        <span class="help-block label label-danger">{{ $errors->first('contacto') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('telefono_contacto') ? 'has-error' : '' }} col-md-6">
                                                        <input type="text" class="form-control" min="4" name="telefono_contacto" value="{{ old('telefono_contacto') }}" placeholder="Numero Telefonico del Contacto">
                                                        <span class="help-block label label-danger">{{ $errors->first('telefono_contacto') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('correo_contacto') ? 'has-error' : '' }} col-md-12">
                                                        <input type="email" class="form-control" min="4" name="correo_contacto" value="{{ old('correo_contacto') }}" placeholder="Email del Contacto">
                                                        <span class="help-block label label-danger">{{ $errors->first('correo_contacto') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('comuna_id') ? 'has-error' : '' }} col-md-12">
                                                        <label>Comuna a la que pertenece la Empresa:</label>
                                                        <select class="form-control" name="comuna_id" required="required">
                                                          <option selected="selected" disabled="disabled">-- Seleccione --</option>
                                                          <optgroup>
                                                                 @foreach($comunas as $comuna)
                                                                   <option value="{{ $comuna->id }}" {{ old('comuna_id') == $comuna->id ? 'selected' : '' }}>{{ $comuna->name }}</option>
                                                                 @endforeach
                                                          </optgroup>
                                                        </select>
                                                        <span class="help-block label label-danger">{{ $errors->first('comuna_id') }}</span>
                                                  </div>
                                                  <div class="form-group{{ $errors->has('pagina_web') ? 'has-error' : '' }} col-md-12">
                                                        <input type="url" class="form-control" min="4" name="pagina_web" value="{{ old('pagina_web') }}" placeholder="Pagina Web de la Empresa">
                                                        <span class="help-block label label-danger">{{ $errors->first('pagina_web') }}</span>
                                                  </div>
                                          </div>
                                                    <!-- /.box-body -->

                                                  <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                                    <button type="reset" class="btn btn-default">Cancelar</button>
                                                  </div>
                                            </form>
                                 </div>

                        </div>

                        <div class="col-md-6">
                                    <div class="box box-success">
                                      <div class="box-header with-border">
                                        <h3 class="box-title">Listado de Empresas</h3>
                                      </div>
                                      <!-- /.box-header -->
                                      <div class="box-body">
                                        <table class="table table-bordered" id="lista">
                                          <thead>
                                                <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Empresa</th>
                                                <th>Razon Social</th>
                                                <th>Fecha de Registro</th>
                                                <th>Acciones</th>
                                                </tr>
                                          </thead>
                                           <tbody>
                                            @foreach($empresas as $emp)
                                            <tr>
                                              <td>{{ $loop->iteration}}</td>
                                              <td>{{ $emp->nombre_fantasia }}</td>
                                              <td>{{ $emp->razon_social }}</td>
                                              <td>{{ $emp->created_at }}</td>
                                              <td><button class="btn btn-danger"><i class="fa fa-close"></i></button></td>
                                            </tr>
                                            @endforeach
                                           </tbody>
                                      </table>
                                      </div>
                                      <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                           </div>

                  </div>
</section>
@endsection