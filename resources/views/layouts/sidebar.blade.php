<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('admin/dist/img/avatar.png')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                @if(Auth::user()->rol_id==0)
                <p>Administrador</p>
                @else
                <p>Miembro</p>
                @endif
                <a href="#"><i class="fa fa-circle text-success"></i> En Linea</a>
            </div>
        </div>

@if(Auth::user()->rol_id==0)
         
        <ul class="sidebar-menu" data-widget="tree">
                <li class="header"><strong>Menus de Navegacion</strong></li>
                <li class="treeview">
                    <a href="#">
    <i class="fa fa-exchange"></i> <span>Configuraciones</span>
    <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('calles.index') }}"><i class="fa  fa-road"></i>Calles</a></li>
                        <li><a href="{{ route('comunas.index') }}"><i class="fa fa-bank"></i>Comunas</a></li>
                        <li><a href="{{ route('etnias.index') }}"><i class="fa  fa-odnoklassniki-square"></i>Etnias</a></li>
                        <li><a href="{{ route('etarios.index') }}"><i class="fa fa-male"></i>Etarios</a></li>
                        <li><a href="{{ route('estados_civiles.index') }}"><i class="fa fa-file-text-o"></i>Estados Civiles</a></li>
                        <li><a href="{{ route('nacionalidades.index') }}"><i class="fa fa-flag-o"></i>Nacionalidades</a></li>
                        <li><a href="{{ route('parentescos.index') }}"><i class="fa fa-group"></i>Parentescos</a></li>
                        <li><a href="{{ route('poblaciones.index') }}"><i class="fa fa-map-marker"></i>Poblaciones</a></li>
                        <li><a href="{{ route('periocidades.index') }}"><i class="fa fa-safari"></i>Periodos</a></li>
                        <li><a href="{{ route('regiones.index') }}"><i class="fa fa-map-o"></i>Regiones</a></li>
                        <li><a href="{{ route('unidades.index') }}"><i class="fa fa-circle-o"></i>Unidades</a></li>
    
                        <!-- <li><a href="/valor"><i class="fa fa-circle-o"></i>Valor Inicial</a></li>-->
                    </ul>
                </li>
    
                <li class="treeview">
                    <a href="#">
    <i class="fa fa-users"></i> <span>Vecinos</span>
    <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('integrantes.create') }}"><i class="fa fa-user-plus"></i>Registrar Vecino</a></li>
                        <li><a href="{{ route('integrantes.index') }}"><i class="fa fa-users"></i>Listado de Vecinos</a></li>
                    </ul>
                </li>
    
                <li class="treeview">
                    <a href="#">
    <i class="fa  fa-sitemap"></i> <span>Familias</span>
    <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('familias.index') }}"><i class="fa fa-user-plus"></i>Gestion de Familias</a></li>
                    </ul>
                </li>
    
                <li class="treeview">
                    <a href="#">
    <i class="fa fa-credit-card"></i> <span>Tarjetas</span>
    <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
    </a>
                    <ul class="treeview-menu">
                        <li><a href="../../index.html"><i class="fa fa-plus"></i>Creacion de Tarjeta</a></li>
                        <li><a href="../../index.html"><i class="fa fa-money"></i>Procesar Pagos Tarjetas</a></li>
                        <li><a href="../../index.html"><i class="fa fa-check-square-o"></i>Asignacion de Tarjetas</a></li>
                    </ul>
                </li>
    
                <li class="treeview">
                    <a href="#">
    <i class="fa fa-building-o"></i> <span>Empresas</span>
    <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('empresas.index') }}"><i class="fa fa-tasks"></i>Gestion de Empresas</a></li>
                        <li><a href="{{ route('productos.index') }}"><i class="fa fa-bar-chart-o"></i>Gestion de Productos</a></li>
                    </ul>
                </li>
    
                <li class="treeview">
                    <a href="#">
    <i class="fa fa-calendar-check-o "></i> <span>Eventos</span>
    <span class="pull-right-container">
      <i class="fa fa-angle-left pull-right"></i>
    </span>
    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('tipo_eventos.index') }}"><i class="fa fa-calendar"></i>Tipos de Eventos</a></li>
                        <li><a href="{{ route('eventos.index') }}"><i class="fa fa-calendar-plus-o"></i>Registro de Eventos</a></li>
                    </ul>
                </li>
    
            </ul>



                @else
<ul class="sidebar-menu" data-widget="tree">
            <li class="header"><strong>Menus de Navegacion</strong></li>
                <li class="treeview">
                        <a href="#"><i class="fa fa-user"></i><span>Cuenta de usuario</span>
                         <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ url('perfil') }}"><i class="fa  fa-black-tie"></i>Informacion del usuario</a></li>
                            </ul>
                </li>
            
                <li class="treeview">
                        <a href="#"><i class="fa fa-users"></i> <span>Familia</span>
                         <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ route('familia.user') }}"><i class="fa fa-user-plus"></i>Gestion de familia</a></li>
                            </ul>
                </li>

                <li class="treeview">
                        <a href="#"><i class="fa fa-credit-card"></i> <span>Pagos y Solicitudes</span>
                          <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                            <ul class="treeview-menu">
                                        <li><a href="{{ route('integrantes.create') }}"><i class="fa fa-sign-in"></i>Procesar Pago</a></li>
                                        <li><a href="{{ route('integrantes.index') }}"><i class="fa fa-sign-in"></i>Procesar Solicitud</a></li>
                            </ul>
                </li>

                <li class="treeview">
                        <a href="#"><i class="fa fa-calendar-check-o"></i> <span>Eventos</span>
                          <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                            <ul class="treeview-menu">
                                    <li><a href="{{ route('eventos.listado') }}"><i class="fa fa-calendar"></i>Listado de Eventos</a></li>
                            </ul>
                </li>

                
</ul>
@endif
 </section>
    <!-- /.sidebar -->
</aside>