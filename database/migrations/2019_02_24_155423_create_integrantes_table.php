<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntegrantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('integrantes', function (Blueprint $table) {

            $table->increments('id');
            $table->string('rut');
            $table->string('nombres');
            $table->string('apellidos');
            $table->date('fecha_nacimiento')->nullable();
            $table->enum('sexo',['M','F'])->nullable();
            $table->unsignedInteger('estado_civil_id')->nullable();
            $table->foreign('estado_civil_id')->references('id')->on('estados_civils');
            $table->unsignedInteger('parentesco_id')->nullable();
            $table->foreign('parentesco_id')->references('id')->on('parentescos');
            $table->string('telf_movil')->nullable();
            $table->string('telf_fijo')->nullable();
            $table->unsignedInteger('etnia_id')->nullable();
            $table->foreign('etnia_id')->references('id')->on('etnias');
            $table->unsignedInteger('etario_id')->nullable();
            $table->foreign('etario_id')->references('id')->on('etarios');
            $table->unsignedInteger('nacionalidad_id')->nullable();
            $table->foreign('nacionalidad_id')->references('id')->on('nacionalidades');
            $table->string('inscripcion')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('integrantes');
    }
}
