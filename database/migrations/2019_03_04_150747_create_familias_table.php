<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamiliasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('familias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cod_familia');
            $table->string('direccion')->nullable();
            $table->integer('numero')->nullable();
            $table->string('blok')->nullable();
            $table->string('depto')->nullable();
            $table->unsignedInteger('unidad_id')->nullable();
            $table->foreign('unidad_id')->references('id')->on('unidades');
            $table->string('manzana')->nullable();
            $table->string('sitio')->nullable();
            $table->unsignedInteger('poblacion_id')->nullable();
            $table->foreign('poblacion_id')->references('id')->on('poblaciones');
            $table->unsignedInteger('comuna_id')->nullable();
            $table->foreign('comuna_id')->references('id')->on('comunas');
            $table->unsignedInteger('calle_id')->nullable();
            $table->foreign('calle_id')->references('id')->on('calles');
            $table->unsignedInteger('region_id')->nullable();
            $table->foreign('region_id')->references('id')->on('regiones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('familias');
    }
}
