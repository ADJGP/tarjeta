<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('tipo_evento_id');
            $table->foreign('tipo_evento_id')->references('id')->on('tipo_eventos');
            $table->integer('cupos');
            $table->date('fec_ini');
            $table->date('fec_fin');
            $table->string('observacion');
            $table->unsignedInteger('periocidad_id');
            $table->foreign('periocidad_id')->references('id')->on('periocidades');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
